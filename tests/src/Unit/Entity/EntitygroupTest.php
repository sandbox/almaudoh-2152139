<?php

/**
 * @file
 * Contains \Drupal\Tests\entitygroup\Unit\Entity\EntitygroupTest
 */

namespace Drupal\Tests\entitygroup\Unit\Entity;

use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \Drupal\entitygroup\Entity\Entitygroup
 * @group Entitygroup
 */
class EntitygroupTest extends UnitTestCase {
  /**
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

//    $this->request = $this->getMockBuilder('Symfony\Component\HttpFoundation\Request');
  }

  /**
   * @covers ::getCurrentBaseRouteName()
   */
  public function testGetCurrentBaseRouteName() {
    $entitygroup = Entitygroup::create([
      'name' => $this->getRandomGenerator()->sentences(2),
      'description' => $this->getRandomGenerator()->paragraphs(1),
      'egid' => $this->randomMachineName(),
      'uuid' => $this->randomMachineName(),
      'uid' => $this->randomMachineName(),
      'type' => $this->randomMachineName(),
      'langcode' => 'en',
    ]);


  }
}
 