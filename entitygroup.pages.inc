<?php

/**
 * @file entitygroup.pages.inc
 *
 * This file contains pre render hooks for themes.
 */

/** Prepares variables for user templates.
 *
 * Default template: entitygroup.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - entitygroup: The entitygroup.
 */
function template_preprocess_entitygroup(&$variables) {
  $entitygroup = $variables['elements']['#entitygroup'];

  // Helpful $content variable for templates.
  foreach (element_children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // Preprocess fields.
//  field_attach_preprocess($entitygroup, $variables['elements'], $variables);

}
