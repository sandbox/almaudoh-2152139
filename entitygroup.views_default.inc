<?php
  
/**
 * @file
 * Default views for entitygroups
 */
function entitygroup_views_default_views() {
	$view = new view;
	$view->name = 'entity_groups';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'entitygroup';
	$view->human_name = 'Entity Groups';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
	
	/* Display: Master */
	$handler = $view->new_display('default', 'Master', 'default');
	$handler->display->display_options['title'] = 'Entity Groups';
	$handler->display->display_options['access']['type'] = 'perm';
	$handler->display->display_options['access']['perm'] = 'administer entitygroups';
	$handler->display->display_options['cache']['type'] = 'none';
	$handler->display->display_options['query']['type'] = 'views_query';
	$handler->display->display_options['query']['options']['query_comment'] = FALSE;
	$handler->display->display_options['exposed_form']['type'] = 'basic';
	$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
	$handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
	$handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
	$handler->display->display_options['pager']['type'] = 'full';
	$handler->display->display_options['pager']['options']['items_per_page'] = '40';
	$handler->display->display_options['pager']['options']['offset'] = '0';
	$handler->display->display_options['pager']['options']['id'] = '0';
	$handler->display->display_options['pager']['options']['expose']['items_per_page'] = TRUE;
	$handler->display->display_options['pager']['options']['expose']['items_per_page_options'] = '5, 10, 20, 40, 60, 100';
	$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
	$handler->display->display_options['style_plugin'] = 'table';
	$handler->display->display_options['style_options']['columns'] = array(
			'egid' => 'egid',
			'views_bulk_operations_1' => 'views_bulk_operations_1',
			'name' => 'name',
			'description' => 'description',
			'name_1' => 'name_1',
			'type' => 'type',
			'nothing' => 'nothing',
			'nothing_1' => 'nothing_1',
	);
	$handler->display->display_options['style_options']['default'] = '-1';
	$handler->display->display_options['style_options']['info'] = array(
			'egid' => array(
					'sortable' => 0,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'views_bulk_operations_1' => array(
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'name' => array(
					'sortable' => 1,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'description' => array(
					'sortable' => 1,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'name_1' => array(
					'sortable' => 1,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'type' => array(
					'sortable' => 1,
					'default_sort_order' => 'asc',
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'nothing' => array(
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
			'nothing_1' => array(
					'align' => '',
					'separator' => '',
					'empty_column' => 0,
			),
	);
	$handler->display->display_options['style_options']['override'] = 1;
	$handler->display->display_options['style_options']['sticky'] = 1;
	$handler->display->display_options['style_options']['empty_table'] = 0;
	/* Relationship: Entity Group: Owner uid */
	$handler->display->display_options['relationships']['owner']['id'] = 'owner';
	$handler->display->display_options['relationships']['owner']['table'] = 'entitygroup';
	$handler->display->display_options['relationships']['owner']['field'] = 'owner';
	$handler->display->display_options['relationships']['owner']['required'] = 1;
	/* Field: Entity Group: Entity Group ID */
	$handler->display->display_options['fields']['egid']['id'] = 'egid';
	$handler->display->display_options['fields']['egid']['table'] = 'entitygroup';
	$handler->display->display_options['fields']['egid']['field'] = 'egid';
	$handler->display->display_options['fields']['egid']['exclude'] = TRUE;
	$handler->display->display_options['fields']['egid']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['external'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['egid']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['egid']['alter']['more_link'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['egid']['alter']['html'] = 0;
	$handler->display->display_options['fields']['egid']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['egid']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['egid']['hide_empty'] = 0;
	$handler->display->display_options['fields']['egid']['empty_zero'] = 0;
	$handler->display->display_options['fields']['egid']['hide_alter_empty'] = 1;
	$handler->display->display_options['fields']['egid']['separator'] = '';
	$handler->display->display_options['fields']['egid']['format_plural'] = 0;
	/* Field: Bulk operations: Entity Group */
	$handler->display->display_options['fields']['views_bulk_operations_1']['id'] = 'views_bulk_operations_1';
	$handler->display->display_options['fields']['views_bulk_operations_1']['table'] = 'entitygroup';
	$handler->display->display_options['fields']['views_bulk_operations_1']['field'] = 'views_bulk_operations';
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['external'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['more_link'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['alter']['html'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['views_bulk_operations_1']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['views_bulk_operations_1']['hide_empty'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['empty_zero'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['hide_alter_empty'] = 1;
	$handler->display->display_options['fields']['views_bulk_operations_1']['vbo']['entity_load_capacity'] = '10';
	$handler->display->display_options['fields']['views_bulk_operations_1']['vbo']['operations'] = array(
			'action::views_bulk_operations_delete_item' => array(
					'selected' => 0,
					'use_queue' => 0,
					'skip_confirmation' => 0,
					'override_label' => 0,
					'label' => '',
			),
			'action::system_message_action' => array(
					'selected' => 0,
					'use_queue' => 0,
					'skip_confirmation' => 0,
					'override_label' => 0,
					'label' => '',
			),
			'action::views_bulk_operations_script_action' => array(
					'selected' => 0,
					'use_queue' => 0,
					'skip_confirmation' => 0,
					'override_label' => 0,
					'label' => '',
			),
			'action::views_bulk_operations_modify_action' => array(
					'selected' => 0,
					'use_queue' => 0,
					'skip_confirmation' => 0,
					'override_label' => 0,
					'label' => '',
					'settings' => array(
							'show_all_tokens' => 1,
							'display_values' => array(
									'_all_' => '_all_',
							),
					),
			),
			'action::views_bulk_operations_argument_selector_action' => array(
					'selected' => 0,
					'skip_confirmation' => 0,
					'override_label' => 0,
					'label' => '',
					'settings' => array(
							'url' => '',
					),
			),
			'action::system_goto_action' => array(
					'selected' => 0,
					'use_queue' => 0,
					'skip_confirmation' => 0,
					'override_label' => 0,
					'label' => '',
			),
			'action::system_send_email_action' => array(
					'selected' => 0,
					'use_queue' => 0,
					'skip_confirmation' => 0,
					'override_label' => 0,
					'label' => '',
			),
	);
	$handler->display->display_options['fields']['views_bulk_operations_1']['vbo']['enable_select_all_pages'] = 1;
	$handler->display->display_options['fields']['views_bulk_operations_1']['vbo']['display_type'] = '0';
	$handler->display->display_options['fields']['views_bulk_operations_1']['vbo']['display_result'] = 1;
	$handler->display->display_options['fields']['views_bulk_operations_1']['vbo']['merge_single_action'] = 0;
	$handler->display->display_options['fields']['views_bulk_operations_1']['vbo']['force_single'] = 0;
	/* Field: Entity Group: Name */
	$handler->display->display_options['fields']['name']['id'] = 'name';
	$handler->display->display_options['fields']['name']['table'] = 'entitygroup';
	$handler->display->display_options['fields']['name']['field'] = 'name';
	$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['name']['alter']['make_link'] = 1;
	$handler->display->display_options['fields']['name']['alter']['path'] = '[current-page:url]/[egid]/edit';
	$handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['name']['alter']['external'] = 0;
	$handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['name']['alter']['more_link'] = 0;
	$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['name']['alter']['html'] = 0;
	$handler->display->display_options['fields']['name']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['name']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['name']['hide_empty'] = 0;
	$handler->display->display_options['fields']['name']['empty_zero'] = 0;
	$handler->display->display_options['fields']['name']['hide_alter_empty'] = 1;
	/* Field: Entity Group: Description */
	$handler->display->display_options['fields']['description']['id'] = 'description';
	$handler->display->display_options['fields']['description']['table'] = 'entitygroup';
	$handler->display->display_options['fields']['description']['field'] = 'description';
	$handler->display->display_options['fields']['description']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['description']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['description']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['description']['alter']['external'] = 0;
	$handler->display->display_options['fields']['description']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['description']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['description']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['description']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['description']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['description']['alter']['more_link'] = 0;
	$handler->display->display_options['fields']['description']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['description']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['description']['alter']['html'] = 0;
	$handler->display->display_options['fields']['description']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['description']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['description']['hide_empty'] = 0;
	$handler->display->display_options['fields']['description']['empty_zero'] = 0;
	$handler->display->display_options['fields']['description']['hide_alter_empty'] = 1;
	/* Field: User: Name */
	$handler->display->display_options['fields']['name_1']['id'] = 'name_1';
	$handler->display->display_options['fields']['name_1']['table'] = 'users';
	$handler->display->display_options['fields']['name_1']['field'] = 'name';
	$handler->display->display_options['fields']['name_1']['relationship'] = 'owner';
	$handler->display->display_options['fields']['name_1']['label'] = 'Owner';
	$handler->display->display_options['fields']['name_1']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['external'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['name_1']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['name_1']['alter']['more_link'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['name_1']['alter']['html'] = 0;
	$handler->display->display_options['fields']['name_1']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['name_1']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['name_1']['hide_empty'] = 0;
	$handler->display->display_options['fields']['name_1']['empty_zero'] = 0;
	$handler->display->display_options['fields']['name_1']['hide_alter_empty'] = 1;
	$handler->display->display_options['fields']['name_1']['link_to_user'] = 1;
	$handler->display->display_options['fields']['name_1']['overwrite_anonymous'] = 0;
	$handler->display->display_options['fields']['name_1']['format_username'] = 1;
	/* Field: Entity Group: Group type (bundle) */
	$handler->display->display_options['fields']['type']['id'] = 'type';
	$handler->display->display_options['fields']['type']['table'] = 'entitygroup';
	$handler->display->display_options['fields']['type']['field'] = 'type';
	$handler->display->display_options['fields']['type']['label'] = 'Group type';
	$handler->display->display_options['fields']['type']['alter']['alter_text'] = 0;
	$handler->display->display_options['fields']['type']['alter']['text'] = '[type]';
	$handler->display->display_options['fields']['type']['alter']['make_link'] = 0;
	$handler->display->display_options['fields']['type']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['type']['alter']['external'] = 0;
	$handler->display->display_options['fields']['type']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['type']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['type']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['type']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['type']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['type']['alter']['more_link'] = 0;
	$handler->display->display_options['fields']['type']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['type']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['type']['alter']['html'] = 0;
	$handler->display->display_options['fields']['type']['element_label_colon'] = 1;
	$handler->display->display_options['fields']['type']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['type']['hide_empty'] = 0;
	$handler->display->display_options['fields']['type']['empty_zero'] = 0;
	$handler->display->display_options['fields']['type']['hide_alter_empty'] = 1;
	/* Field: Global: Custom text */
	$handler->display->display_options['fields']['nothing']['id'] = 'nothing';
	$handler->display->display_options['fields']['nothing']['table'] = 'views';
	$handler->display->display_options['fields']['nothing']['field'] = 'nothing';
	$handler->display->display_options['fields']['nothing']['label'] = '';
	$handler->display->display_options['fields']['nothing']['alter']['text'] = 'edit';
	$handler->display->display_options['fields']['nothing']['alter']['make_link'] = 1;
	$handler->display->display_options['fields']['nothing']['alter']['path'] = '[current-page:url]/[egid]/edit';
	$handler->display->display_options['fields']['nothing']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['external'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['nothing']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['nothing']['alter']['more_link'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['nothing']['alter']['html'] = 0;
	$handler->display->display_options['fields']['nothing']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['nothing']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['nothing']['hide_empty'] = 0;
	$handler->display->display_options['fields']['nothing']['empty_zero'] = 0;
	$handler->display->display_options['fields']['nothing']['hide_alter_empty'] = 0;
	/* Field: Global: Custom text */
	$handler->display->display_options['fields']['nothing_1']['id'] = 'nothing_1';
	$handler->display->display_options['fields']['nothing_1']['table'] = 'views';
	$handler->display->display_options['fields']['nothing_1']['field'] = 'nothing';
	$handler->display->display_options['fields']['nothing_1']['label'] = '';
	$handler->display->display_options['fields']['nothing_1']['alter']['text'] = 'delete';
	$handler->display->display_options['fields']['nothing_1']['alter']['make_link'] = 1;
	$handler->display->display_options['fields']['nothing_1']['alter']['path'] = '[current-page:url]/[egid]/delete';
	$handler->display->display_options['fields']['nothing_1']['alter']['absolute'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['external'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['replace_spaces'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['trim_whitespace'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['nl2br'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['word_boundary'] = 1;
	$handler->display->display_options['fields']['nothing_1']['alter']['ellipsis'] = 1;
	$handler->display->display_options['fields']['nothing_1']['alter']['more_link'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['strip_tags'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['trim'] = 0;
	$handler->display->display_options['fields']['nothing_1']['alter']['html'] = 0;
	$handler->display->display_options['fields']['nothing_1']['element_label_colon'] = FALSE;
	$handler->display->display_options['fields']['nothing_1']['element_default_classes'] = 1;
	$handler->display->display_options['fields']['nothing_1']['hide_empty'] = 0;
	$handler->display->display_options['fields']['nothing_1']['empty_zero'] = 0;
	$handler->display->display_options['fields']['nothing_1']['hide_alter_empty'] = 0;
	/* Contextual filter: Entity Group: Group type (bundle) */
	$handler->display->display_options['arguments']['type']['id'] = 'type';
	$handler->display->display_options['arguments']['type']['table'] = 'entitygroup';
	$handler->display->display_options['arguments']['type']['field'] = 'type';
	$handler->display->display_options['arguments']['type']['default_action'] = 'empty';
	$handler->display->display_options['arguments']['type']['title_enable'] = 1;
	$handler->display->display_options['arguments']['type']['title'] = '%1';
	$handler->display->display_options['arguments']['type']['default_argument_type'] = 'fixed';
	$handler->display->display_options['arguments']['type']['default_argument_skip_url'] = 0;
	$handler->display->display_options['arguments']['type']['summary']['number_of_records'] = '0';
	$handler->display->display_options['arguments']['type']['summary']['format'] = 'default_summary';
	$handler->display->display_options['arguments']['type']['summary_options']['items_per_page'] = '25';
	$handler->display->display_options['arguments']['type']['specify_validation'] = 1;
	$handler->display->display_options['arguments']['type']['validate']['type'] = 'php';
	$handler->display->display_options['arguments']['type']['validate_options']['code'] = 'return entitygroup_access(entitygroup_type_load($argument), NULL, TRUE);';
	$handler->display->display_options['arguments']['type']['validate']['fail'] = 'empty';
	$handler->display->display_options['arguments']['type']['glossary'] = 0;
	$handler->display->display_options['arguments']['type']['limit'] = '0';
	$handler->display->display_options['arguments']['type']['case'] = 'ucwords';
	$handler->display->display_options['arguments']['type']['transform_dash'] = 0;
	$handler->display->display_options['arguments']['type']['break_phrase'] = 0;
	/* Filter criterion: Entity Group: Description */
	$handler->display->display_options['filters']['description']['id'] = 'description';
	$handler->display->display_options['filters']['description']['table'] = 'entitygroup';
	$handler->display->display_options['filters']['description']['field'] = 'description';
	$handler->display->display_options['filters']['description']['operator'] = 'word';
	$handler->display->display_options['filters']['description']['exposed'] = TRUE;
	$handler->display->display_options['filters']['description']['expose']['operator_id'] = 'description_op';
	$handler->display->display_options['filters']['description']['expose']['label'] = 'Description';
	$handler->display->display_options['filters']['description']['expose']['operator'] = 'description_op';
	$handler->display->display_options['filters']['description']['expose']['identifier'] = 'description';
	$handler->display->display_options['filters']['description']['expose']['required'] = 0;
	$handler->display->display_options['filters']['description']['expose']['multiple'] = FALSE;
	/* Filter criterion: Entity Group: Name */
	$handler->display->display_options['filters']['name']['id'] = 'name';
	$handler->display->display_options['filters']['name']['table'] = 'entitygroup';
	$handler->display->display_options['filters']['name']['field'] = 'name';
	$handler->display->display_options['filters']['name']['operator'] = 'word';
	$handler->display->display_options['filters']['name']['exposed'] = TRUE;
	$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
	$handler->display->display_options['filters']['name']['expose']['label'] = 'Name';
	$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
	$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
	$handler->display->display_options['filters']['name']['expose']['required'] = 0;
	$handler->display->display_options['filters']['name']['expose']['multiple'] = FALSE;
	
	/* Display: Page */
	$handler = $view->new_display('page', 'Page', 'admin_page');
	$handler->display->display_options['defaults']['arguments'] = FALSE;
	$handler->display->display_options['path'] = 'admin/config/entitygroups';
	$handler->display->display_options['menu']['type'] = 'normal';
	$handler->display->display_options['menu']['title'] = 'Entitygroups';
	$handler->display->display_options['menu']['weight'] = '0';
	$handler->display->display_options['menu']['name'] = 'management';
	$handler->display->display_options['menu']['context'] = 0;
	
	$views[$view->name] = $view;
  
	return $views;
}