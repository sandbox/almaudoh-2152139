<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Access\EntitygroupAccessCheck.
 */

namespace Drupal\entitygroup\Access;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Access\AccessException;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Provides a custom access check callback for entitygroups.
 */
class EntitygroupAccessCheck {

  /**
   * Filters a list of entitygroups to those accessible by $account based on the
   * entity group access settings.
   *
   * @param \Drupal\entitygroup\Entity\Entitygroup[] $entitygroups
   *   The list of entitygroups.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account object for which access is to be checked - defaults to the
   *   current user
   *
   * @return \Drupal\entitygroup\Entity\Entitygroup[]
   */
  public static function filter(array $entitygroups, AccountInterface $account = NULL) {
    return array_filter(array_map(function($group) use ($account) {
      if (static::access($group, $account)->isAllowed()) {
        return $group;
      }
      else
        return null;
    }, $entitygroups));
  }

  /**
   * Checks access with permissions incorporated.
   *
   * @param \Drupal\entitygroup\Entity\Entitygroup $entitygroup
   *   The entitygroup entity.
   * @param string $perm
   *   The additional permission to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *    The account whose access is being checked.
   *
   * @return bool
   *   Whether access is allowed or forbidden.
   *
   * @see \Drupal\entitygroup\Access\EntitygroupAccessCheck::access()
   * @see \Drupal\entitygroup\Access\EntitygroupAccessCheck::accessCheck()
   */
  public static function checkWithPermission($entitygroup, $perm, AccountInterface $account = NULL) {
    if (!isset($account)) $account = \Drupal::currentUser();
    return EntitygroupAccessCheck::access($entitygroup)->isAllowed() && $account->hasPermission($perm);
  }

  /**
   * Checks bundle access with permissions incorporated.
   *
   * @param \Drupal\entitygroup\Entity\EntitygroupType $entitygrouptype
   *   The entitygroup entity.
   * @param string $perm
   *   The additional permission to check.
   * @param \Drupal\Core\Session\AccountInterface $account
   *    The account whose access is being checked.
   *
   * @return bool
   *   Whether access is allowed or forbidden.
   *
   * @see \Drupal\entitygroup\Access\EntitygroupAccessCheck::accessBundle()
   * @see \Drupal\entitygroup\Access\EntitygroupAccessCheck::accessCheck()
   */
  public static function checkBundleWithPermission($entitygrouptype, $perm, AccountInterface $account = NULL) {
    if (!isset($account)) $account = \Drupal::currentUser();
    return EntitygroupAccessCheck::accessBundle($entitygrouptype)->isAllowed() && $account->hasPermission($perm);
  }

  /**
   * Determines if an account has access to an entity group based on configured
   * settings.
   *
   * @param \Drupal\entitygroup\Entity\Entitygroup $entitygroup
   *    The entitygroup entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *    The account whose access is being checked.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *
   * @see \Drupal\entitygroup\Access\EntitygroupAccessCheck::accessCheck()
   */
  public static function access($entitygroup, AccountInterface $account = NULL) {
    if (is_string($entitygroup)) $entitygroup = Entitygroup::load($entitygroup);
    if ($entitygroup && $entitygroup instanceof Entitygroup) {
      /** @var \Drupal\entitygroup\Entity\EntitygroupType $type */
      $type = EntitygroupType::load($entitygroup->bundle());
      return static::accessCheck($type, $entitygroup, $account);
    }
    else {
      throw new AccessException('Entitygroup for access check not defined.');
    }
  }

  /**
   * Checks if a user has access to create an entity group type (bundle).
   *
   * @param \Drupal\entitygroup\Entity\EntitygroupType $entitygrouptype
   *    The entity group type (bundle).
   * @param \Drupal\Core\Session\AccountInterface $account
   *    The account whose access is being checked.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *
   * @see \Drupal\entitygroup\Access\EntitygroupAccessCheck::accessCheck()
   */
  public static function accessBundle($entitygrouptype, AccountInterface $account = NULL) {
    if (is_string($entitygrouptype)) $entitygrouptype = EntitygroupType::load($entitygrouptype);
    if ($entitygrouptype && $entitygrouptype instanceof EntitygroupType) {
      return static::accessCheck($entitygrouptype, NULL, $account);
    }
    else {
      throw new AccessException('Entitygroup type for access check not defined.');
    }
  }

  /**
   * Determines if an account has access to an entitygroup based on configured
   * settings.
   *
   * @param \Drupal\entitygroup\Entity\EntitygroupType $type
   *    The entity group type (bundle).
   * @param \Drupal\entitygroup\Entity\Entitygroup $group
   *    The entitygroup entity.
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   */
  protected static function accessCheck(EntitygroupType $type, Entitygroup $group = NULL, AccountInterface $account = NULL) {
    if (!isset($account)) $account = \Drupal::currentUser();

    // User #1 has all privileges:
    if ($account->id() == 1) {
      return AccessResult::allowed();
    }
    switch ($type->access['mode']) {
      case 'owner':
        // Only the owner of the entity group has access.
        return AccessResult::allowedIf(!isset($group) || $account->id() == $group->get('uid'));

      case 'user':
        // A selected set of users have access.
        $allowed_users = Tags::explode($type->access['user']);
        return AccessResult::allowedIf(in_array($account->getUsername(), $allowed_users));

      case 'role':
        // Users belonging to a drupal role have access.
        $access_result = AccessResult::neutral();
        foreach (array_filter($type->access['role']) as $rid => $role) {
          $access_result->orIf(AccessResult::allowedIf(in_array($role, $account->getRoles())));
        }
        return $access_result;

      case 'group':
        // Members of an entity group have access.
        // @todo Needs fixing.
        $items = $account->{$type->access['group']['fieldname']};
        if ($items instanceof \Traversable) {
          foreach ($items as $item) {
            if (strpos($item->groups, "|{$type->access['group']['entitygroup']}|") !== FALSE) return TRUE;
          }
        }
        return FALSE;

      // All users of the site have access (including anonymous users). Default.
      case 'all':
      default:
        return AccessResult::allowed();
    }
  }

}
