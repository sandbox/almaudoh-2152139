<?php
/**
 * Contains \Drupal\entitygroup\EntitygroupController
 */

namespace Drupal\entitygroup;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entitygroup\Entity\EntitygroupType;
use Drupal\entitygroup\Entity\Entitygroup;

/**
 * General purpose controller for entitygroup module
 * @todo Update entitygroup.routing.yml to use this controller
 */
class EntitygroupController {

  use StringTranslationTrait;

  /**
   * Title callback for various menu entries
   */
  public function entitygroupTitle($action, $entitygrouptype = NULL, $entitygroup = NULL) {
    if (is_string($entitygrouptype)) $entitygrouptype = EntitygroupType::load($entitygrouptype);
    if (is_numeric($entitygroup)) $entitygroup = Entitygroup::load($entitygroup);

    $t_args = array(
      '@name' => isset($entitygroup->name) ? $entitygroup->name->value : '',
      '@type' => isset($entitygrouptype) ? $entitygrouptype->label() : $this->t('Entity Group'),
    );
    switch ($action) {
      case 'add type':
        return t('Add new entitygroup type', $t_args);

      case 'edit type':
        return t('Edit "@type" entitygroup type', $t_args);

      case 'view type':
        return t('View "@type" entitygroup type', $t_args);

      case 'delete type':
        return t('Delete "@type" entitygroup type', $t_args);

      case 'list type':
        return t('Entitygroup type list', $t_args);

      case 'add group':
        return t('Add new @type', $t_args);

      case 'edit group':
        return t('Edit "@name" @type', $t_args);

      case 'view group':
        return t('View "@name" @type', $t_args);

      case 'delete group':
        return t('Delete "@name" @type', $t_args);

      case 'list group':
        return t('@type list', $t_args);

      default:
        return '';
    }
  }
} 