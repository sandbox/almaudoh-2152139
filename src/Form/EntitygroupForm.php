<?php

/**
 * @file
 * Definition of Drupal\entitygroup\Form\EntitygroupForm.
 */

namespace Drupal\entitygroup\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Form controller for the entitygroup create / edit forms.
 */
class EntitygroupForm extends ContentEntityForm {

  /**
   * @var \Drupal\entitygroup\Entity\Entitygroup
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\entitygroup\Entity\Entitygroup $entitygroup */
    $entitygroup = $this->entity;
    $entitygrouptype = $this->getEntitygroupType();
    
    $form['help'] = array(
      '#type' => 'help',
      '#description' => 'An entitygroup provides a means of grouping drupal entities on your site. '
        . 'Entitygroups can be utilized for creating user groups, organic groups and whatever thingy group you like. '
        . 'It provides functionality for tagging (like taxonomy), group searching and bulk operations (e.g. promote all users in this group)',
    );
    $t_args = array(
      '%name' => $entitygrouptype->name,
      '%name-lower' => strtolower($entitygrouptype->name),
      '%title' => $entitygrouptype->label(),
      '%title-lower' => strtolower($entitygrouptype->label()),
    );
    $form['#t_args'] = $t_args;
    $form['identity'] = array(
      '#type' => 'fieldset',
      '#title' => t('Identification'),
    );
    $form['identity']['type_display'] = array(
      '#title' => t('Group Type'),
      '#type' => 'item',
      '#markup' => $entitygrouptype->label(),
    );
    $form['identity']['type'] = array(
      '#type' => 'value',
      '#value' => $entitygrouptype->name,
    );
    $form['identity']['name'] = array(
      '#type' => 'textfield',
      '#title' => 'Name',
      '#maxlength' => 255,
      '#default_value' => $entitygroup->name->value,
      '#description' => t('The human-readable name of this %title-lower. This text will be used for grouping, tagging and/or searching. <br/>'
          . 'It is recommended that this name be concise and descriptive e.g. "Class 1", "Batch A", "My contacts", "Power users", etc. <br/>'
          . 'This name must be unique.', $t_args),
      '#required' => TRUE,
    );
    $form['identity']['description'] = array(
      '#title' => t('Description'),
      '#type' => 'textarea',
      '#default_value' => $entitygroup->description->value,
      '#description' => t('A brief description of this %title-lower. This text may contain more elaborate descriptions of the %title-lower '
            . 'including its purpose and its components.', $t_args),
      '#required' => TRUE,
    );

//    $form['members'] = array(
//        '#value' => '',
//    );
//
//    $members = entitygroup_get_members($entitygroup);
//    $headers = array();
//    $rows = array();
//    foreach ($members as $entitytype => $entity_ids) {
//      // Load all entities of this type that belong to this entitygroup
//      $entities = entity_load_multiple($entitytype, $entity_ids);
//      foreach ($entities as $entity) {
//        $form[$entity->id()] = entity_view($entity, 'summary');
//      }
//    }
    return $form;
  }
  
  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // Customized access check for 'Delete' button
    $actions = parent::actions($form, $form_state);
    $actions['delete']['#access'] = EntitygroupAccessCheck::checkWithPermission($this->entity, 'delete entitygroup', $this->currentUser());
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    if ($this->entity->isNew()) {
      // Update uid field for new entitygroups
      $this->entity->uid = $this->currentUser()->id();
    }
    $status = $this->entity->save();
    $form_state->setValue('egid', $this->entity->id());

    $t_args = $form['#t_args'] + array('@name' => $this->entity->name->value);
    if ($status == SAVED_NEW || $status == SAVED_UPDATED) {
      drupal_set_message(t('Group %title-lower "@name" successfully saved.', $t_args));
    }
    else {
      drupal_set_message(t('Could not save %title-lower "@name"', $t_args), 'warning');
    }
    $form_state->setRedirectUrl($this->entity->getCurrentBaseUrl());
  }
  
  /**
   * {@inheritdoc}
   */
  public function delete(array $form, FormStateInterface $form_state) {
    // Redirect to confirm form.
    $form_state->setRedirectUrl($this->entity->getUrlForOperation('delete'));
  }

  /**
   * Returns the EntitygroupType for this Entitygroup.
   *
   * @return \Drupal\entitygroup\Entity\EntitygroupType
   */
  protected function getEntitygroupType() {
    if (EntitygroupType::typeExists($this->entity->type->value)) {
      return EntitygroupType::load($this->entity->type->value);
    }
    else {
      $type = $this->getRequest()->get('entitygrouptype');
      if (is_string($type)) $type = EntitygroupType::load($type);
      return $type;
    }
  }

  /**
   * Renders the fields in the given entity.
   *
   * This is basically preservation of an old function no more needed.
   *
   * @param $entitytype
   * @param $entities
   *
   * @todo Remove this later.
   */
  protected function renderEntityFields($entitytype, $entities) {
    // Setup headers for the fields in this entity type bundle
    foreach (field_info_instances($entitytype) as $bundle => $fields) {
      foreach ($fields as $fieldname => $field) {
        $headers[$entitytype][$bundle][$fieldname] = $field->label;
      }
    }
    //       field_attach_load($entitytype, $entities);
    foreach ($entities as $entity) {
      $row = array();
      foreach ($headers[$entitytype][$entity->bundle()] as $fieldname => $fieldlabel) {
        $row[$fieldname] = array('data' => field_view_field($entity, $fieldname));
        $row[$fieldname]['data']['#label_display'] = 'hidden'; // Ensure labels are hidden
        if ($row[$fieldname]['data']['#formatter'] == 'image') {
          // Image field types should have smaller images 'entitygroup_tablerow' image is predefined
          foreach (element_children($row[$fieldname]['data']) as $item) {
            $row[$fieldname]['data'][$item]['#image_style'] = 'entitygroup_tablerow';
          }
        }
        $row[$fieldname]['data']['#label_display'] = 'hidden'; // Ensure labels are hidden
      }
      $rows[$entitytype][$entity->bundle()][$entity->id()] = $row;
    }

    dpm($rows);
    // Render the tables for this $entitytype
    foreach ($rows[$entitytype] as $bundle => $row) {
      $form['members'][$entitytype][$bundle] = array(
        '#theme' => 'table',
        '#header' => $headers[$entitytype][$bundle],
        '#rows' => $rows[$entitytype][$bundle],
        '#empty' => t('No group members'),
      );
    }
  }

}