<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Form\EntitygroupDeleteForm.
 */

namespace Drupal\entitygroup\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Provides the entitygroup delete confirmation form.
 */
class EntitygroupDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * @var \Drupal\entitygroup\Entity\Entitygroup
   */
  protected $entity;

  /**
   * Arguments for $this->t() method.
   *
   * @var array
   */
  private $t_args;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('You are about to delete %name!!', $this->getTArgs());
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // @todo Need to make this 'destination'-smart.
    return $this->entity->getCurrentBaseUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Are you sure you want to delete this %type? You will lose all functionality associated
        with this group. This action cannot be undone.', $this->getTArgs());
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   * @todo delete / update should also clean-up / sync the fields attached to the entitygroup
   */
  public function submit(array $form, FormStateInterface $form_state) {
    // Delete the entitygroup and its sub-components.
    $this->entity->delete();
  	drupal_set_message(t('The "%name" %type has been deleted.', $this->getTArgs()));
    $this->logger('entitygroup')->notice('Deleted entitygroup "%name" of type %type.', $this->getTArgs());
    // Clear the cache so an anonymous user sees that his entitygroup was deleted.
//     Cache::invalidateTags(array('content' => TRUE));
    $form_state->setRedirectUrl($this->entity->getCurrentBaseUrl());
  }
  
  /**
   * Helper function to provide consistent arguments for user messages.
   */
  private function getTArgs() {
    if (!isset($this->t_args)) {
      $type = EntitygroupType::load($this->entity->type->value);
      $this->t_args = array('%name' => $this->entity->name->value, '%type' => $type->title);
    }
    return $this->t_args;
  }

}
