<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Form\EntitygroupTypeDeleteForm.
 */

namespace Drupal\entitygroup\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entitygroup\Entity\Entitygroup;

/**
 * Provides the entitygroup delete confirmation form.
 */
class EntitygroupTypeDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Add options for dropping user group types.
    $form = parent::buildForm($form, $form_state);
    $form['options'] = array(
      '#type' => 'checkboxes',
      '#options' => array(
        'delete_groups' => $this->t('Delete all entitygroups of type %type', array('%type' => $this->entity->label())),
      ),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the entitygroup type %title?', array('%title' => $this->entity->label()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entitygroup.type_edit', ['entitygrouptype' => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Are you sure you want to delete this entitygroup type. All entity groups of this type
        will be inaccessible. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete entitygroup');
  }

  /**
   * {@inheritdoc}
   * @todo Deletions should also clean up the fields and field settings that are attached
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Delete the entitygroup type.
    $this->entity->delete();

    // Delete associated entities if specified.
    if ($form_state->getValue(['options', 'delete_groups'])) {
      // Load each entity and then delete. This will take some time.
      /** @var \Drupal\entitygroup\Entity\Entitygroup[] $groups */
      $groups = Entitygroup::loadByType($this->entity->name);
      foreach ($groups as $group) {
        $group->delete();
      }
    }
    // Rebuild menu to update entity group type links.
    \Drupal::service('router.builder')->rebuild();
    $t_args = array(
      '%name' => $this->entity->label(),
    );
    drupal_set_message(t('The entitygroup type "%name" has been deleted.', $t_args));
    $this->logger('entitygroup')->notice('Deleted entitygroup type "%name".', $t_args + array('links' => $this->l($this->t('view'), new Url('entitygroup.type_list'))));
    $form_state->setRedirect('entitygroup.type_list');
  }
}
