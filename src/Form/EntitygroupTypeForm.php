<?php

/**
 * @file
 * Definition of Drupal\entitygroup\Form\EntitygroupTypeForm.
 */

namespace Drupal\entitygroup\Form;

use Drupal\Component\Utility\String;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Form controller for the entitygroup create / edit forms.
 */
class EntitygroupTypeForm extends EntityForm {

  /**
   * @var \Drupal\entitygroup\Entity\EntitygroupType
   */
  protected $entity;

  /**
   * A flag that menu should be rebuilt after entity type is saved.
   *
   * @var bool
   */
  private $rebuildMenu = FALSE;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\entitygroup\Entity\EntitygroupType $entitygroup_type */
    $entitygroup_type = $this->entity;
    $form['help'] = array(
      '#type' => 'help',
      // @todo: Improve this help documentation.
      '#description' => 'An entitygroup type determines the types of entitygroups you can create.',
    );
    $form['identity'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Identification'),
    );
    $form['identity']['label'] = array(
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $entitygroup_type->label,
      '#description' => $this->t('The human-readable label of this entitygroup type. This text will be displayed as part of the list on the <em>Create Entitygroup</em> page. It is recommended that this name begins with a capital letter and contain only letters, numbers and <strong>spaces</strong>. This name must be unique.'),
      '#required' => TRUE,
    );
    $form['identity']['name'] = array(
      '#title' => $this->t('Name'),
      '#type' => 'machine_name',
      '#default_value' => $entitygroup_type->name,
      '#maxlength' => 32,
      '#required' => TRUE,
      '#description' => $this->t('The machine name of this content type. This text will be used for constructing the URL of the <em>Create Entitygroup</em> page for this content type. This name must contain only lowercase letters, numbers and underscores. The underscores will be replaced with hyphens when constructing the URL. This name must be unique.'),
      '#disabled' => $entitygroup_type->locked,
      '#machine_name' => array(
        'exists' => [get_class($entitygroup_type), 'typeExists'],
        'source' => array('identity','label'),
        'label' => $this->t('Type'),
      ),
    );
    $form['identity']['description'] = array(
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $entitygroup_type->description,
      '#description' => $this->t('A brief description of this entity group type. This text will be displayed as part of the list on the <em>Create Entitygroup</em> page.'),
    );
    $form['advanced'] = array(
      '#type' => 'vertical_tabs',
      '#attributes' => array('class' => array('entity-meta')),
      '#weight' => 99,
    );
    $form['access'] = array(
      '#type' => 'details',
      '#title' => $this->t('Access settings'),
      '#description' => $this->t('Access permission settings for this entitygroup type'),
      '#open' => FALSE,
      '#group' => 'advanced',
      '#tree' => TRUE,
    );
    $form['access']['mode'] = array(
      '#title' => $this->t('Entity group access mode.'),
      '#description' => $this->t('Choose which mode will be used to determine which users have access to this entitygroup type.'),
      '#type' => 'select',
      '#default_value' => $entitygroup_type->access['mode'],
      '#required' => TRUE,
      '#options' => EntitygroupType::getAccessModes(),
    );

    // Dependent form elements for each type of access mode. Naming convention:
    // use '{modename}'.

    // Selected set of users (extended user autocomplete box).
    $form['access']['user'] = array(
      '#title' => $this->t('Usernames'),
      '#type' => 'textfield',
      '#default_value' => $entitygroup_type->access['mode'] == 'user' ? $entitygroup_type->access['user'] : '',
      '#description' => $this->t('Type the name of the users here (comma-separated). Autocomplete is active for the last username in the list.'),
      '#autocomplete_route_name' => 'user.autocomplete',
      '#states' => array(
        'visible' => array(
          'select[name="access[mode]"]' => array('value' => 'user'),
        ),
      ),
    );
    // Users belonging to a drupal role.
    $form['access']['role'] = array(
      '#title' =>$this->t('User roles'),
      '#type' => 'checkboxes',
      '#default_value' => ($entitygroup_type->access['mode'] == 'role' && $entitygroup_type->access['role']) ? $entitygroup_type->access['role'] : array(),
      '#description' => $this->t('Select the drupal role(s) that can access this entitygroup.'),
      '#options' => array_map(function ($role) { return String::checkPlain($role->label()); }, user_roles()),
      '#states' => array(
        'visible' => array(
          'select[name="access[mode]"]' => array('value' => 'role'),
        ),
      ),
    );

    // Users belonging to an existing entitygroup.
    // The entitygroups must be tied to user profiles.
    $form['access']['group'] = array(
      '#title' => $this->t('Entity group'),
      '#type' => 'container',
      '#description' => $this->t('Users belonging to an entitygroup. Select the user field to be used, and then the group the user must belong to.'),
      '#tree' => TRUE,
      '#states' => array(
        'visible' => array(
          'select[name="access[mode]"]' => array('value' => 'group'),
        ),
      ),
    );

    // Add information for the remaining two modes.
    $form['access']['owner'] = array(
      '#type' =>  'value',
      '#value' => $this->currentUser()->id(),
    );
    $form['access']['all'] = array(
      '#type' => 'value',
      '#value' => TRUE,
    );

    // Get user fields that are entitygroup type.
    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $fields */
    $fields = array_filter(\Drupal::entityManager()->getFieldDefinitions('user', 'user'), function(FieldDefinitionInterface $value) {
      return $value->getType() == 'entitygroup';
    });

    $field_names = array();

    // Permissions based on the members of a specific entitygroup. This requires
    // all the user entities that have entitygroup fields of this group.
    // @todo Consider pulling this out to it's own method somewhere.
    foreach ($fields as $field_name => $field_definition) {
      // Setup options for choosing which field to check by.
      $field_names[$field_name] = $field_definition->getLabel() . ' (' . $field_name . ')';

      $entitygroup_type_name = $field_definition->getSetting('entitygroup_type');
      $options = array('--Select--');
      foreach (Entitygroup::loadByType($entitygroup_type_name) as $group) {
        $options[$group->id()] = $group->name->value;
      }
      $form['access']['group'][$field_name] = array(
        '#title' => $this->t('Specify group'),
        '#type' => 'select',
        '#default_value' => $entitygroup_type->access['mode'] == 'group' &&
          $entitygroup_type->access['group']['field_name'] == $field_name ? $entitygroup_type->access['group']['entitygroup'] : '',
        '#description' => $this->t('Users belonging to an entitygroup. Select the entitygroup here.'),
        '#options' => $options,
        '#states' => array(
          'visible' => array(
            ':input[name="access[group][field_name]"]' => array('value' => $field_name),
          ),
        ),
      );
    }
    if ($fields) {
      $form['access']['group']['field_name'] = array(
        '#title' => $this->t('Choose the user field'),
        '#type' => 'radios',
        // force it to have value of last option so the dependent selects above
        // don't misbehave.
        '#default_value' => ($entitygroup_type->access['mode'] == 'group' &&
            $entitygroup_type->access['group']['field_name']) ? $entitygroup_type->access['group']['field_name'] : '',
        '#options' => array_map(function($field_name) { return $field_name->getLabel(); }, $fields),
        '#weight' => -2,
      );
    }
    else {
      $form['access']['group']['field_name'] = array(
        '#markup' => $this->t('No User entitygroup field attached. You must !link to the User entity type',
            ['!link' => $this->l($this->t('attach an entitygroup field'), new Url('field_ui.overview_user'))]),
        '#type' => 'markup',
        '#weight' => -2,
      );
    }

    // Add menu options tree.
    $form['menu'] = array(
      '#type' => 'details',
      '#group' => 'advanced',
      '#title' => $this->t('Menu Options'),
      '#parents' => ['menu'],
      '#tree' => TRUE,
      '#attached' => array(
        'library' => array('menu/drupal.menu'),
      ),
    );
    // Option to add a custom menu.
    $form['menu']['enabled'] = array(
      '#title' => $this->t('Add a custom menu item for this entitygroup type'),
      '#type' => 'checkbox',
      '#default_value' => $entitygroup_type->menu['enabled'],
    );
    // Other options for the entitygroup type.
    $form['menu']['options'] = array(
      '#parents' => ['menu'],
      '#type' => 'container',
      '#states' => array(
        'visible' => array(
          ':input[name="menu[enabled]"]' => array('checked' => TRUE),
        ),
      ),
    );
    // Custom menu path for the entitygroup.
    $form['menu']['options']['title'] = array(
      '#title' => $this->t('Menu title'),
      '#type' => 'textfield',
      '#default_value' => isset($entitygroup_type->menu['title']) ? $entitygroup_type->menu['title'] : ucfirst($entitygroup_type->label),
      '#description' => $this->t('The menu title as it appears to the users.'),
    );
    $form['menu']['options']['description'] = array(
      '#title' => $this->t('Menu description'),
      '#type' => 'textarea',
      '#rows' => 1,
      '#default_value' => isset($entitygroup_type->menu['description']) ? $entitygroup_type->menu['description'] : $entitygroup_type->description,
      '#description' => $this->t('The menu description which will show as a tooltip.'),
    );
    $form['menu']['options']['path'] = array(
      '#title' => $this->t('Base path'),
      '#type' => 'textfield',
      '#default_value' => isset($entitygroup_type->menu['path']) ? $entitygroup_type->menu['path'] : $entitygroup_type->name,
      '#description' => $this->t('The base path of the menu entry'),
      '#field_prefix' => $this->url('<none>', [], ['absolute' => TRUE]),
      '#size' => 20,
    );
    $menu_parent_selector = \Drupal::service('menu.parent_form_selector');
    $default = $entitygroup_type->menu['parent'] ?: 'main:';
    $form['menu']['options']['parent'] = $menu_parent_selector->parentSelectElement($default,  '', NULL);
    $form['menu']['options']['parent']['#title'] = $this->t('Parent menu');
    $form['menu']['options']['parent']['#attributes']['class'][] = 'menu-parent-select';
    $form['menu']['options']['weight'] = array(
    	'#title' => $this->t('Menu weight'),
    	'#type' => 'weight',
    	'#description' => $this->t('The lower the weight the higher/further left it will appear.'),
    	'#default_value' => isset($entitygroup_type->menu['weight']) ? $entitygroup_type->menu['weight']: 0,
    );
    // Option to restrict members of entitygroup to entities of a specific type.
    $options = array_map(function ($entity_type) { return $entity_type->getLabel(); }, \Drupal::entityManager()->getDefinitions());
    asort($options);
    // Other options for the entitygroup type.
    $form['options'] = array(
      '#type' => 'details',
      '#title' => $this->t('Additional Options'),
      '#group' => 'advanced',
    );
    $form['options']['restrict'] = array(
    	'#type' => 'select',
    	'#title' => $this->t('Restrict group members to selected entities'),
    	'#default_value' => isset($entitygroup_type->restrict) ? $entitygroup_type->restrict : '',
    	'#options' => $options,
      '#group' => 'options',
    	'#multiple' => TRUE,
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();
    
    $t_args = array(
      '%name' => $this->entity->label(),
    );
    if ($status == SAVED_UPDATED) {
      drupal_set_message($this->t('The entitygroup type "%name" has been updated.', $t_args));
    }
    else if ($status == SAVED_NEW) {
      drupal_set_message($this->t('The entitygroup type "%name" has been added.', $t_args));
      $this->logger('entitygroup')->notice($this->t('Added entitygroup type "%name".'),
        $t_args + ['link' => $this->entity->link($this->t('View'))]);
    }
    
    // Rebuild menu if the menu settings have changed.
    if ($this->rebuildMenu) {
      \Drupal::service('router.builder_indicator')->setRebuildNeeded();
      // @todo Temporary hack till https://www.drupal.org/node/2383935 is fixed.
      drupal_flush_all_caches();
      drupal_set_message($this->t('Menu router rebuilt.'));
    }
    $form_state->setRedirect('entitygroup.type_list');
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $form, FormStateInterface $form_state) {
    // Redirect to delete confirm form.
    $form_state->setRedirect('entitygroup.type_delete', ['entitygrouptype' => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    /** @var \Drupal\entitygroup\Entity\Entitygroup $entity */
    $old = clone $entity;
    parent::copyFormValuesToEntity($entity, $form, $form_state);
    // @todo Should locked property be user configurable?
    $entity->locked = TRUE; // $values['locked'];
    // Flag that menu needs rebuild if any of the menu options changed.
    $this->rebuildMenu = ($entity->menu['enabled'] != $old->menu['enabled']
                       || $entity->menu['path']    != $old->menu['path']
                       || $entity->menu['parent']  != $old->menu['parent']
                       || $entity->menu['weight']  != $old->menu['weight']
                       || $entity->menu['title']   != $old->menu['title']
                       || $entity->menu['description'] != $old->menu['description']);
    return $entity;
  }

}
