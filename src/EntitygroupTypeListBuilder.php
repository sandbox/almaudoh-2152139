<?php

/**
 * @file
 * Definition of Drupal\entitygroup\EntitygroupTypeListBuilder.
 */

namespace Drupal\entitygroup;

use Drupal\Component\Utility\String;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of entitygroup categories.
 */
class EntitygroupTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = t('Name');
    $header['description'] = t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['name'] = $entity->link(NULL, 'canonical');
    $row['description'] = String::checkPlain($entity->description);
    return $row + parent::buildRow($entity);
  }

}
