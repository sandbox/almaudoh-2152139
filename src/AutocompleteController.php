<?php

/**
 * @file
 * Contains \Drupal\entitygroup\AutocompleteController.
 */
namespace Drupal\entitygroup;

use Drupal\Component\Utility\String;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\entitygroup\Entity\Entitygroup;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller class for specialized entitygroup autocompletion
 */
class AutocompleteController {

  /**
   * Returns response for the entitygroup autocompletion.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object containing the search string.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the comma-separated autocomplete suggestions
   *   for entity groups
   */
  public function autocompleteGroup(Request $request) {
    // Use entity query to pull group matches.
    $result = \Drupal::entityQuery('entitygroup')
      ->condition('name', $request->query->get('q'), 'STARTS_WITH')
      ->execute();
    $matches = array();
    if ($result) {
      /** @var \Drupal\entitygroup\Entity\Entitygroup[] $groups */
      $groups = EntitygroupAccessCheck::filter(Entitygroup::loadMultiple($result));
      foreach ($groups as $group) {
        $matches[] = ['value' => $group->label(), 'label' => String::checkPlain($group->label())];
      }
    }
    return new JsonResponse($matches);
  }

}
