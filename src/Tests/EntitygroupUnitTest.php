<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Tests\EntitygroupWebTest
 */

namespace Drupal\entitygroup\Tests;

use Drupal\Component\Utility\Unicode;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\system\Tests\Entity\EntityUnitTestBase;

/**
 * UI integration tests for Entitygroup entity including CRUD.
 *
 * @group Entitygroup
 *
 * @coversClass Entitygroup
 * @coversClass EntitygroupForm
 * @coversClass EntitygroupDeleteForm
 * @coversClass EntitygroupListBuilder
 */
class EntitygroupUnitTest extends EntityUnitTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['entitygroup'];

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
    $this->installEntitySchema('entitygroup');
  }

  /**
   * Tests CRUD for entitygroup types.
   */
  public function testGetMembers() {
    // Create entitygroup.
    $group1 = $this->createEntitygroup('test_group_type', ['name' => 'test_group']);

    // Add an entitygroup field to user entity.
    $field_name = Unicode::strtolower($this->randomMachineName());
    $field_storage_definition = [
      'field_name' => $field_name,
      'entity_type' => 'user',
      'type' => 'entitygroups',
    ];
    $field_storage = FieldStorageConfig::create($field_storage_definition);
    $field_storage->save();
    $field_definition = array(
      'field_name' => $field_storage->getName(),
      'entity_type' => 'user',
      'bundle' => 'user',
      'settings' => [
        'entitygroup_type' => 'test_group_type',
        'allow_add' => TRUE,
      ],
    );
    $field = FieldConfig::create($field_definition);
    $field->save();

    // Create users.
    $user1 = $this->createUser([], ['add entitygroup']);
    $user2 = $this->createUser();

    // Login as a user with permissions to add entitygroups.
    $this->container->get('current_user')->setAccount($user1);

    // Add $user1 and $user2 to $group1.
    $user1->set($field_name, $group1->label());
    $user1->save();
    $user2->set($field_name, $group1->label());
    $user2->save();

    // Create an entity of type test_entity.
    $this->assertTrue($user1->hasField($field_name), 'User 1 has field ' . $field_name);
    $this->assertTrue($user2->hasField($field_name), 'User 2 has field ' . $field_name);

    // Assert that the $group1->getMembers() == 2.
    $members = $group1->getMembers();
    $this->assertEqual(2, count($members['user']), 'Get members returns the right number of entitygroup members.');

    // Assert that each member matches.
    $this->assertEqual($members['user'], [$user1->id() => $user1->id(), $user2->id() => $user2->id()], 'Get members returns the right entitygroup members');

  }

  /**
   * Creates a test entitygroup of the specified entitygroup type.
   *
   * @param string $type
   *   The entitygroup type of the entitygroup to be created. It will be created
   *   if it does not already exist.
   * @param array $values
   *   Array of default values to be provided.
   *
   * @return \Drupal\entitygroup\Entity\EntityGroup
   */
  public function createEntitygroup($type, $values) {
    if (EntitygroupType::typeExists($type)) {
      $type = EntitygroupType::load($type);
    }
    else {
      $type = EntitygroupType::create([
        'name' => $type,
        'description' => $this->randomString(20),
        // Allow access to all.
        'access' => ['mode' => 'all'],
      ]);
      $type->enforceIsNew();
      $type->save();
    }
    $entitygroup = Entitygroup::create($values + array(
        'name' => $this->randomString(),
        'description' => $this->randomString(20),
        'type' => $type->id(),
      ));
    $entitygroup->enforceIsNew();
    $entitygroup->save();
    return $entitygroup;
  }

}
