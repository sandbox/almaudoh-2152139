<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Tests\EntitygroupWebTest
 */

namespace Drupal\entitygroup\Tests;

use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\simpletest\WebTestBase;
use Drupal\user\Entity\User;
use Symfony\Component\Validator\Constraints\False;
use Symfony\Component\Validator\Constraints\True;

/**
 * UI integration tests for Entitygroup entity including CRUD.
 *
 * @group Entitygroup
 *
 * @coversClass Entitygroup
 * @coversClass EntitygroupForm
 * @coversClass EntitygroupDeleteForm
 * @coversClass EntitygroupListBuilder
 */
class EntitygroupWebTest extends WebTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['entitygroup', 'field', 'field_ui', 'menu_ui'];

  /**
   * Tests CRUD for entitygroup types.
   */
  public function testCrudEntitygroupType() {
    $user = $this->drupalCreateUser(['administer entitygroups', 'view entitygroup']);
    $this->drupalLogin($user);

    // Test the list page is working.
    $this->drupalGet('/admin/structure/entitygroup');
    $this->assertResponse(200);
    $this->assertRaw('Entitygroup types');
    $this->assertRaw('<th>Name</th>', '"Name" column found.');
    $this->assertRaw('<th>Description</th>', '"Description" column found');
    $this->assertRaw('There is no Entitygroup type yet.');

    // Test the create page is working and successfully created a new entitygroup
    // type.
    $edit = [
      'label' => $this->randomString(10),
      'name' => 'test_machine_name',
      'description' => $this->randomString(30),
      'access[mode]' => 'user',
      'access[user]' => $user->id(),
      'menu[enabled]' => TRUE,
      'menu[title]' => $this->randomString(20),
      'menu[path]' => $this->randomMachineName(),
//      'restrict[][]' => 'user',
    ];
    $this->drupalPostForm('/admin/structure/entitygroup/addtype', $edit, t('Save'));
    $this->assertResponse(200);
    $type = EntitygroupType::load('test_machine_name');
    $this->assertNotNull($type, 'Entitygroup type saved.');

    // Test to edit the entitygroup that was just added.
    $edit['description'] = 'changed the description in this test.';
    unset($edit['name']);
    $this->drupalPostForm('/admin/structure/entitygroup/manage/test_machine_name', $edit, t('Save'));
    $this->assertResponse(200);
    $type = EntitygroupType::load('test_machine_name');
    $this->assertEqual($edit['description'], $type->description, 'Changed the entitygroup type description.');

    // Test the entitygroup type custom menu link.
    $this->drupalGet('/'.$edit['menu[path]']);
    $this->assertResponse(200);
    $this->assertRaw($edit['menu[title]']);
    $this->clickLink('Add new ' . $edit['menu[title]']);

    // Test to delete the entitygroup type.
    $this->drupalGet('/admin/structure/entitygroup/manage/test_machine_name');
    $this->assertResponse(200);
    $this->clickLink(t('Delete'));
    $this->assertUrl('/admin/structure/entitygroup/manage/test_machine_name/delete');
    $this->drupalPostForm('/admin/structure/entitygroup/manage/test_machine_name/delete', [], t('Delete entitygroup'));
    $this->assertResponse(200);
    $type = EntitygroupType::load('test_machine_name');
    $this->assertNull($type, 'Entitygroup type deleted.');
  }

  /**
   * Tests user entity integration for entitygroup entities.
   *
   * @todo Fix this test.
   */
  public function _testUserEntityIntegration() {
    $admin = $this->drupalCreateUser(['administer entitygroup', 'administer users']);
    $this->drupalLogin($admin);
    $user = $this->drupalCreateUser();

    // Create a test entitygroup entity.
    $entitygroup_type = Entitygroup::create([
      'name' => 'entitygroup_type',
      'label' => 'Test Entity Type',
      'description' => 'Testing that the right type is displayed on form.'
    ]);
    $entitygroup_type->save();
    $field_storage = FieldStorageConfig::create([
      'field_name' => 'test_field',
      'entity_type' => 'entitygroup',
      'type' => 'string',

    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'entity_type' => 'entitygroup',
      'bundle' => 'entitygroup_type',
      'settings' => [
        '' => '',
      ],
    ]);
    $field->save();

    $entitygroup = Entitygroup::create([
      'uid' => $user->id(),
      'type' => 'entitygroup_type',
      'langcode' => 'en'
    ]);
    $entitygroup->set('test_field', 'Test Field');
    $entitygroup->save();

//    User::load($user->id(), TRUE)->save();

    // Tests that entitygroup widgets are available on the user profile form.
    $this->drupalGet('user/' . $user->id() . '/edit');
    $this->assertRaw('Test Entity Type');
    $this->assertRaw('user_extensions[test_entity_type][test_field][0][value][date]');

  }

  public function _testAdminForm() {
    // Test that anonymous user cannot access admin page.
    $this->drupalGet('admin/config/people/entitygroup/settings');
    $this->assertResponse(403);
    $this->assertRaw('Access denied');

    // Login with admin permission and create new user extensions.
    $user = $this->drupalCreateUser(['administer entitygroup']);
    $this->drupalLogin($user);
    $edit = [
      'label' => 'Foo User Type',
      'name' => 'foo_user_type',
      'description' => $this->randomString(30),
    ];
    $this->drupalPostForm('/admin/structure/entitygroup/add', $edit, t('Save'));
    $this->assertResponse(200);

    // Configure the form to place form widgets on the profile page
    $this->drupalPostForm('admin/config/people/entitygroup/settings', ['form_placement' => 'profile'], t('Save settings'));
    $this->assertResponse(200);
    $this->assertRaw('xxx');

    // Check profile page and confirm widget.
    $this->drupalGet('user/' . $user->id() . '/edit');
    $this->assertResponse(200);
  }

  /**
   * @todo
   */
  public function _testViewsIntegration() {

  }

}
