<?php

/**
 * Definition of Drupal\entitygroup\EntitygroupListBuilder.
 */

namespace Drupal\entitygroup;

use Drupal\Component\Utility\String;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;
use Drupal\user\Entity\User;
use Drupal\views\Entity\View;

/**
 * Provides a listing of entitygroup categories.
 */
class EntitygroupListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader()
  {
    $header = array(
      'name' => t('Name'),
      'description' => t('Description'),
      'owner' => t('Owner'),
      'members' => t('Members'),
    );
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entitygroup) {
    // Only show this entitygroup if the user has view access and the type is correct.
    if (EntitygroupAccessCheck::access($entitygroup)->isAllowed()) {
      $row = array(
        'name' => $entitygroup->link(),
        'description' => String::checkPlain($entitygroup->description->value),
        'owner' => $entitygroup->uid->entity->label(),
        'members' => array_sum(array_map(function ($item) { return count($item); }, $entitygroup->getMembers())),
      );
      return $row + parent::buildRow($entitygroup);
    }
    else {
      return array();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $type = \Drupal::request()->get('entitygrouptype');
    if (is_string($type)) $type = EntitygroupType::load($type);
    // Render using views if views is available.
    if (false) {
//    if ($output = $this->renderView($type)) {
      return $output;
    }
    else {
      // Fallback to default list if views is not available.
      return $this->renderFallback($type);
    }
  }

  /**
   * Renders the entitygroup list using the built-in view.
   *
   * This will fail if either views module is unavailable or the view does not
   * exist.
   *
   * @see config/install/views.view.entity_groups.yml for the default view.
   */
  public function renderView($entitygrouptype) {
    // This view has contextual filters %1/%2 where %1 is entitygroup type name and
    // %2 is entitygroup owner uid - supplied only when we filter by owner uid
    // as is obtainable when $entitygrouptype access model is owner
    if ($this->moduleHandler()->moduleExists('views')) {
      /** @var \Drupal\views\Entity\View $view */
      $view = View::load('entity_groups');
      $display_id = 'default';
      if ($view) {
        $args[] = $entitygrouptype->name;
        if ($entitygrouptype->access == 'owner') $args[] = \Drupal::currentUser()->id();

        // Replace tokens placed in the view rewrite links - the dynamic base paths
        // This is a hack I don't like!!!
        $token = \Drupal::token();
//        $view->display[$display_id]->display_options['fields']['name']['alter']['path'] = $token->replace($view->display['default']->display_options['fields']['name']['alter']['path']);
//        $view->display[$display_id]->display_options['fields']['nothing']['alter']['path'] = $token->replace($view->display['default']->display_options['fields']['nothing']['alter']['path']);
//        $view->display[$display_id]->display_options['fields']['nothing_1']['alter']['path'] = $token->replace($view->display['default']->display_options['fields']['nothing_1']['alter']['path']);
//        $content = $view->executeDisplay($display_id, $args);
        $content = $view->preview($display_id, $args);
//        $content = views_embed_view('entity_groups', $display_id, $args);

        return $content;
        // Replace tokens I placed in the view
        $content = $token->replace($content);

        $build['content'] = array(
          'dummy' => array('#markup' => $content)
        );
        return $build;
      }
      else {
        return array();
      }
    }
    else {
      return array();
    }

  }

  /**
   * Renders the default entitygroup list table in case views isn't available.
   *
   * @param \Drupal\entitygroup\Entity\Entitygroup $type
   *   The entitygroup type whose list of entitygroups is to be displayed.
   *
   * @return array
   *   A drupal render array.
   */
  protected function renderFallback($type) {
    $build = array(
      '#theme' => 'table',
      '#header' => $this->buildHeader(),
      '#rows' => array(),
      '#empty' => $this->t('No %group created yet.', array('%group' => $type->label())),
    );
    foreach (Entitygroup::loadByType($type->name) as $entity) {
      if ($row = $this->buildRow($entity)) {
        $build['#rows'][$entity->id()] = $row;
      }
    }
    return $build;
  }

}
