<?php

/**
 * @file
 * Definition of Drupal\node\Plugin\views\argument\Nid.
 */

namespace Drupal\entitygroup\Plugin\views\argument;

use Drupal\Component\Utility\String;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\views\Plugin\views\argument\Numeric;

/**
 * Argument handler to accept an entitygroup ID.
 *
 * @PluginID("entitygroup_egid")
 */
class Egid extends Numeric {

  /**
   * Override the behavior of title(). Get the title of the entitygroup.
   */
  public function titleQuery() {
    $titles = array();

    $groups = Entitygroup::loadMultiple($this->value);
    foreach ($groups as $group) {
      $titles[] = String::checkPlain($group->label());
    }
    return $titles;
  }

}
