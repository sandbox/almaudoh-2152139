<?php

/**
 * @file
 * Definition of Drupal\entitygroup\Plugin\views\field\Entitygroup.
 */

namespace Drupal\entitygroup\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;

/**
 * Field handler to provide simple renderer that allows linking to an entitygroup.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("entitygroup")
 */
class Entitygroup extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
    parent::init($view, $display, $options);

    if (!empty($this->options['link_to_entitygroup'])) {
      $this->additional_fields['egid'] = 'egid';
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['link_to_entitygroup'] = array('default' => TRUE, 'bool' => TRUE);
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $form['link_to_entitygroup'] = array(
      '#title' => t('Link this field to its entitygroup'),
      '#description' => t("Enable to override this field's links."),
      '#type' => 'checkbox',
      '#default_value' => $this->options['link_to_entitygroup'],
    );
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * Prepares a link to the entitygroup url.
   *
   * @param string $data
   *   The XSS safe string for the link text.
   * @param \Drupal\views\ResultRow $values
   *   The values retrieved from a single row of a view's query result.
   *
   * @return string
   *   Returns a string for the link text.
   */
  protected function renderLink($data, ResultRow $values) {
    $group = $this->getEntity($values);
    if (!empty($this->options['link_to_entitygroup']) && EntitygroupAccessCheck::checkWithPermission($group, 'view entitygroup') && $data !== NULL && $data !== '') {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = $group->urlInfo()->getPath();
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = $this->getValue($values);
    return $this->renderLink($this->sanitizeValue($value), $values);
  }

}
