<?php

/**
 * @file
 * Contains \Drupal\node\Plugin\views\field\NodeBulkForm.
 */

namespace Drupal\entitygroup\Plugin\views\field;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\system\Plugin\views\field\BulkForm;
use Drupal\Core\Cache\Cache;

/**
 * Defines a node operations bulk form element.
 *
 * @ViewsField("entitygroup_bulk_form")
 */
class EntitygroupBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  public function viewsFormSubmit(&$form, &$form_state) {
    parent::viewsFormSubmit($form, $form_state);
    if ($form_state['step'] == 'views_form_views_form') {
      Cache::invalidateTags(array('content' => TRUE));
    }
  }

}
