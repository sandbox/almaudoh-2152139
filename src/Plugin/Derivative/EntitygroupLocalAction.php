<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Derivative\EntitygroupLocalTask
 */

namespace Drupal\entitygroup\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Provides dynamic local action definitions for entitygroups with custom menu
 * entries.
 */
class EntitygroupLocalAction extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    /** @var \Drupal\entitygroup\Entity\EntitygroupType $type */
    foreach (EntitygroupType::loadMultiple() as $type) {
      if ($type->menu['enabled']) {
        // Implement dynamic local actions for entitygroups with custom menu
        $route_name = "entitygroup.{$type->name}_add";
        $this->derivatives[$route_name] = [
          // @todo translate.
          'title' => 'Add new ' . $type->label(),
          'id' => $route_name,
          'route_name' => $route_name,
          'appears_on' => ["entitygroup.{$type->name}"],
        ] + $base_plugin_definition;
      }
    }
    return $this->derivatives;
  }

}
