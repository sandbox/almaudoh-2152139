<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Derivative\EntitygroupMenuLink
 */

namespace Drupal\entitygroup\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Provides dynamic menu link definitions for entitygroups with custom menu
 * entries.
 *
 * @see \Drupal\entitygroup\Plugin\Menu\EntitygroupMenuLink
 */
class EntitygroupMenuLink extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    /** @var \Drupal\entitygroup\Entity\EntitygroupType $type */
    foreach (EntitygroupType::loadMultiple() as $type) {
      if ($type->menu['enabled']) {
        $route_name = "entitygroup.{$type->name}";
        $this->derivatives[$route_name] = $type->getMenuLink() + [
          'title' => $type->label(),
          'description' => $type->description,
          'route_name' => $route_name,
          'weight' => 0,
          'metadata' => ['entitygrouptype' => $type->name],
        ] + $base_plugin_definition;
      }
    }
    return $this->derivatives;
  }

}
