<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Derivative\EntitygroupLocalTask
 */

namespace Drupal\entitygroup\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Provides dynamic local task definitions for entitygroups with custom menu
 * entries.
 */
class EntitygroupLocalTask extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach (EntitygroupType::loadMultiple() as $type) {
      /** @var \Drupal\entitygroup\Entity\EntitygroupType $type */
      if ($type->menu['enabled'] && isset($type->menu['path'])) {
        // Implement dynamic local tasks using the same convention as entitygroups routes.
        $view_route_name = "entitygroup.{$type->name}_view";
        $this->derivatives[$view_route_name] = [
          'title' => 'View',
          'weight' => -3,
          'route_name' => $view_route_name,
          'base_route' => $view_route_name,
        ] + $base_plugin_definition;

        $edit_route_name = "entitygroup.{$type->name}_edit";
        $this->derivatives[$edit_route_name] = [
          'title' => 'Edit',
          'weight' => -2,
          'route_name' => $edit_route_name,
          'base_route' => $view_route_name,
        ] + $base_plugin_definition;
      }
    }
    return $this->derivatives;
  }

}
