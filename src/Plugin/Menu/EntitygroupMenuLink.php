<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Menu\EntitygroupLocalAction.
 */

namespace Drupal\entitygroup\Plugin\Menu;

use Drupal\Core\Menu\MenuLinkDefault;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Allows default menu link descriptions to be modified by the user.
 */
class EntitygroupMenuLink extends MenuLinkDefault {

  /**
   * {@inheritdoc}
   */
  protected $overrideAllowed = array(
    'menu_name' => 1,
    'parent' => 1,
    'weight' => 1,
    'expanded' => 1,
    'enabled' => 1,
    // Allow title and description to be overridden.
    'description' => 1,
    'title' => 1,
  );

  /**
   * {@inheritdoc}
   */
  public function updateLink(array $new_definition_values, $persist) {
    $new_definition_values = parent::updateLink($new_definition_values, $persist);
    if ($persist) {
      // Update the entitygrouptype menu link properties.
      /** @var \Drupal\entitygroup\Entity\EntitygroupType $entitygrouptype */
      $entitygrouptype = EntitygroupType::load($this->getMetaData()['entitygrouptype']);
      $entitygrouptype->updateMenuLink($new_definition_values);
    }
    return $new_definition_values;
  }


}