<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Menu\Form\EntitygroupMenuLinkForm.
 */

namespace Drupal\entitygroup\Plugin\Menu\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\Form\MenuLinkDefaultForm;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Provides a form to edit entitygroup menu links.
 *
 * This provides the feature to edit the title and description, in contrast to
 * the default menu link form.
 *
 * @see \Drupal\entitygroup\Plugin\Menu\EntitygroupMenuLink
 */
class EntitygroupMenuLinkForm extends MenuLinkDefaultForm {

  /**
   * The edited entitygroup menu link.
   *
   * @var \Drupal\entitygroup\Plugin\Menu\EntitygroupMenuLink
   */
  protected $menuLink;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    // Entitygroup type.
    $entitygrouptype = EntitygroupType::load($this->menuLink->getMetaData()['entitygrouptype']);

    // Put the title field first.
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->menuLink->getTitle(),
      '#weight' => -10,
    );

    $form['description'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Shown when hovering over the menu link.'),
      '#default_value' => $this->menuLink->getDescription(),
      '#weight' => -5,
    );

    $form += parent::buildConfigurationForm($form, $form_state);

    $form['info']['#weight'] = -8;
    $form['path']['#weight'] = -7;

    if (\Drupal::currentUser()->hasPermission('administer entitygroups')) {
      $message = $this->t('This link is provided by the Entitygroup module. The path can be changed by editing the entitygroup type !link', ['!link' => $entitygrouptype->link()]);
      $form['info']['#title'] = $message;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function extractFormValues(array &$form, FormStateInterface $form_state) {
    $definition = parent::extractFormValues($form, $form_state);
    $definition['title'] = $form_state->getValue('title');
    $definition['description'] = $form_state->getValue('description');

    return $definition;
  }

}
