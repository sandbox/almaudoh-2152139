<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Menu\EntitygroupLocalAction.
 */

namespace Drupal\entitygroup\Plugin\Menu;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Defines a local action plugin with a dynamic title.
 */
class EntitygroupLocalAction extends LocalActionDefault {

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    $type = $this->request()->get('entitygrouptype');
    if (is_string($type)) $type = EntitygroupType::load($type);
    if (empty($type)) {
      throw new \InvalidArgumentException('Invalid entitygroup type specified.');
    }
    return $this->t('Add new @type', array('@type' => $type->label()));
  }

  /**
   * Wraps the drupal request service.
   */
  protected function request() {
    return \Drupal::request();
  }

}