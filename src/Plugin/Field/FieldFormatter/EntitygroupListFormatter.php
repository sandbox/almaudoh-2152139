<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Field\FieldFormatter\EntitygroupListFormatter.
 */

namespace Drupal\entitygroup\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entitygroup_list' formatter.
 *
 * @FieldFormatter(
 *   id = "entitygroup_list",
 *   label = @Translation("List of entitygroups"),
 *   field_types = {
 *     "entitygroups"
 *   },
 *   settings = {
 *     "list_type" = "comma-separated"
 *   }
 * )
 */
class EntitygroupListFormatter extends FormatterBase {
  
  /**
   * Translatable labels for the list types settings
   * @var array
   */
  static $list_types;
  
  /**
   * Return the types of list available
   */
  static function getListTypes() {
    if (!isset(static::$list_types)) {
      static::$list_types = array(
        'comma-separated' => t('Comma-separated'),
        'newline-separated' => t('Newline-separated'),
        'htmlbr-separated' => t('HTML<br>-separated'),
      );
    }
    return static::$list_types;
  }
  
  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['listtype'] = array(
      '#type' => 'select',
      '#title' => t('The type of list'),
      '#default_value' => $this->getSetting('list_type'),
      '#options' => static::getListTypes(),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();
    $list_type = $this->getSetting('list_type');

    if (!empty($list_type)) {
      $list_types = static::getListTypes();
      $summary[] = t('List separator: @type', array('@type' => $list_types[$list_type]));
    }
    else {
      $summary[] = t('Default list separator.');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    $element = array();
    $entitygroup_type = $this->getFieldSetting('entitygroup_type');
    switch ($this->getSetting('list_type')) {
      case 'newline-separated':
        foreach ($items as $delta => $item) {
          $element[$delta] = array(
            '#markup' => str_replace(',', "\n", entitygroup_ids_to_tags($item->groups, $entitygroup_type)),
          );
        }
        break;
        
      case 'htmlbr-separated':
        foreach ($items as $delta => $item) {
          $element[$delta] = array(
            '#markup' => str_replace(',', '<br/>', entitygroup_ids_to_tags($item->groups, $entitygroup_type)),
          );
        }
        break;
        
      case 'comma-separated':
      default:
        foreach ($items as $delta => $item) {
          $element[$delta] = array(
            '#markup' => entitygroup_ids_to_tags($item->groups, $entitygroup_type),
          );
        }
        break;
    }
    return $element;
  }

}
