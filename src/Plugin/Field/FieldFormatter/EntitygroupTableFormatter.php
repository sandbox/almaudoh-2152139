<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Field\FieldFormatter\EntitygroupTableFormatter.
 */

namespace Drupal\entitygroup\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Plugin implementation of the 'entitygroup_table' formatter.
 *
 * @FieldFormatter(
 *   id = "entitygroup_table",
 *   label = @Translation("Table of entitygroups"),
 *   field_types = {
 *     "entitygroups"
 *   },
 *   settings = { }
 * )
 */
class EntitygroupTableFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
//   public function settingsForm(array $form, array &$form_state) {
//     $elements['title'] = array(
//       '#type' => 'textfield',
//       '#title' => t('Title to replace basic numeric telephone number display.'),
//       '#default_value' => $this->getSetting('title'),
//     );

//     return $elements;
//   }

  /**
   * {@inheritdoc}
   */
//   public function settingsSummary() {
//     $summary = array();
//     $settings = $this->getSettings();

//     if (!empty($settings['title'])) {
//       $summary[] = t('Link using text: @title', array('@title' => $settings['title']));
//     }
//     else {
//       $summary[] = t('Link using provided telephone number.');
//     }

//     return $summary;
//   }

  /**
   * {@inheritdoc}
   */
//   public function prepareView(array $entities, $langcode, array $items) {
//     $settings = $this->getSettings();
    

//     foreach ($entities as $id => $entity) {
//       foreach ($items[$id] as $item) {
//         // If available, set custom link text.
//         if (!empty($settings['title'])) {
//           $item->title = $settings['title'];
//         }
//         // Otherwise, use telephone number itself as title.
//         else {
//           $item->title = $item->value;
//         }
//       }
//     }
//   }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items) {
    // @todo Should these expensive operations not be in the prepareView() method??
    $element = [];
    $header = [t('Name'), t('Description'), t('Members')];
    foreach ($items as $delta => $item) {
      $ids = explode('|', trim($item->groups, '|'));
      $entitygroups = EntitygroupAccessCheck::filter(Entitygroup::loadMultiple($ids));
      $rows = array();
      // Create list of members also
      foreach ($entitygroups as $entitygroup) {
        $members = $entitygroup->getMembers();
        $count = 0;
        foreach($members as $type => $list) $count += count($list);
        $rows[] = [$entitygroup->name->value, $entitygroup->description->value, $count];
      }

      $element[$delta] = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#header' => $header,
      );
    }
    return $element;
  }

}
