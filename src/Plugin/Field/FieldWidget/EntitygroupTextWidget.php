<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\field\widget\EntitygroupTextWidget.
 */

namespace Drupal\entitygroup\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\entitygroup\Entity\EntitygroupType;


/**
 * Plugin implementation of the 'entitygroup_text' widget.
 *
 * @FieldWidget(
 *   id = "entitygroup_text",
 *   label = @Translation("Comma-separated text"),
 *   description = @Translation("Allow the user to enter groups as comma-separated tag names."),
 *   field_types = {
 *     "entitygroups"
 *   }
 * )
 */
class EntitygroupTextWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $type = EntitygroupType::load($this->getFieldSetting('entitygroup_type'));
    // Those without access shouldn't even see the widget.
    if (!EntitygroupAccessCheck::checkBundleWithPermission($type, 'edit entitygroup')) return array();

    $base = $element;
    $element['groups'] = array(
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]->groups) ? entitygroup_ids_to_tags($items[$delta]->groups, $type->name) : '',
        '#autocomplete_route_name' => 'entitygroup.autocomplete_group',
        '#autocomplete_route_parameters' => array(
            'entitygrouptype' => $type->name,
        ),
    ) + $base;
    return $element;
  }

}
