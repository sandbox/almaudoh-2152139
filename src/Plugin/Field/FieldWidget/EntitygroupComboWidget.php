<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Field\FieldWidget\EntitygroupComboWidget.
 */

namespace Drupal\entitygroup\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;

/**
 * Plugin implementation of the 'entitygroup_combo' widget.
 *
 * @FieldWidget(
 *   id = "entitygroup_combo",
 *   label = @Translation("Combined text & checkboxes"),
 *   description = @Translation("Allow the user to select checkboxes and also type comma-separated text."),
 *   field_types = {
 *     "entitygroups"
 *   },
 * )
 */
class EntitygroupComboWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'combotype' => 'check',
    ) + parent::defaultSettings();
  }

  /**
   * Types of comboboxes to use for selecting groups
   */
  public static function getComboTypes() {
    return array(
      'check' => t('Checkboxes'),
      'list' => t('Listbox'),
      'dropcheck' => t('Dropdown checkboxes'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['combotype'] = array(
      '#type' => 'select',
      '#title' => t('Options list style'),
      '#default_value' => $this->getSetting('combotype'),
      '#required' => TRUE,
      '#options' => self::getComboTypes(),
    );
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = array();

    $combotypes = self::getComboTypes();
    if ($combotype = $combotypes[$this->getSetting('combotype')]) {
      $summary[] = t('Options list style: ') . $combotype;
    }
    else {
      $summary[] = t('Unspecified list style');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $type = EntitygroupType::load($this->getFieldSetting('entitygroup_type'));
    // Those without access shouldn't even see the widget.
    if (!EntitygroupAccessCheck::checkBundleWithPermission($type, 'edit entitygroup')) return array();

    $base = $element;
    $element['entitygroup_wrapper'] = array(
        '#tree' => TRUE,
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
      ) + $base;

    $t_args = array(
      '%in-phrase' => Unicode::strtolower($type->label()),
      '@title' => $type->label(),
    );
    // If dynamic creation of entitygroups is allowed, show the tags textfield.
    if ($this->getFieldSetting('allow_add')) {
      // Needed to define the remote element for states.
      // @todo - is there a standardized way of doing this?
      $parents = array_merge($element['#field_parents'], [$this->fieldDefinition->getName(), $element['#delta'], 'entitygroup_wrapper', 'old_groups']);
      $name = array_shift($parents);
      $name .= '[' . implode('][', $parents) . ']';
      switch ($this->getSetting('combotype')) {
        case 'list':
          // Listboxes are handled slightly differently by the DOM.
          // @todo states not working properly with listbox.
          if ($this->getFieldSetting('multiselect')) {
            $selector = 'select[name="' . $name . '[]"]';
            $condition = array('value' => '_new_');
          }
          else {
            $selector = 'select[name="' . $name . '"]';
            $condition = array('value' => '_new_');
          }
          break;
        case 'dropcheck':
        case 'check':
        default:
          // Checkboxes are handled slightly differently by the DOM.
          if ($this->getFieldSetting('multiselect')) {
            $selector = ':input[name="' . $name . '[_new_]"]';
            $condition = array('checked' => TRUE);
          }
          else {
            $selector = ':input[name="' . $name . '"]';
            $condition = array('value' => '_new_');
          }
      }

      $element['entitygroup_wrapper']['new_groups'] = array(
        '#type' => 'textfield',
        '#title' => t('New'),
        '#description' => t('Type in new %in-phrase here (comma-separated).', $t_args),
        '#size' => 40,
        '#required' => FALSE,
        '#maxlength' => 255,
        '#weight' => 0,
        '#states' => array(
          'visible' => array(
            $selector => $condition,
          ),
        ),
      ) + $base;
      $groups = array('_new_' => t('Add new'));
    }
    else {
      $groups = array();
    }

    foreach (EntitygroupAccessCheck::filter(Entitygroup::loadByType($type->name)) as $group) {
      $groups[$group->id()] = $group->get('name')->value;
    }

    if ($groups) {
      $group_values = isset($items[$delta]->groups) ? array_filter(explode('|', trim($items[$delta]->groups, '|'))) : array();
      // Alter the type to match settings.
      switch ($this->getSetting('combotype')) {
        case 'list':
          $element_type = 'select';
          $default_value = $group_values;
          break;
        case 'dropcheck':
        case 'check':
        default:
          if ($this->getFieldSetting('multiselect')) {
            $element_type = 'checkboxes';
            $default_value = $group_values;
          }
          else {
            $element_type = 'radios';
            $default_value = array_shift($group_values);
          }
      }

      $element['entitygroup_wrapper']['old_groups'] = array(
        '#type' => $element_type,
        '#title' => '',
        '#description' => t('Click here to choose from existing %in-phrase.', $t_args),
        '#options' => $groups,
        '#required' => FALSE,
        '#default_value' => $default_value,
        '#multiple' => $this->getFieldSetting('multiselect'),
        '#weight' => 1,
      ) + $base;
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $element, FormStateInterface $form_state) {
    foreach ($values as $delta => &$item) {
      if (isset($item['entitygroup_wrapper']['new_groups'])) {
        $groups['_new_'] = Tags::explode($item['entitygroup_wrapper']['new_groups']);
      }
      else {
        $groups = array();
      }
      if (isset($item['entitygroup_wrapper']['old_groups'])) {
        // Filter out the 'Add new' option and other unselected options from the
        // results so they don't get saved.
        if (is_array($item['entitygroup_wrapper']['old_groups'])) {
          unset($item['entitygroup_wrapper']['old_groups']['_new_']);
          $groups['_old_'] = array_filter($item['entitygroup_wrapper']['old_groups']);
        }
        else if (is_scalar($item['entitygroup_wrapper']['old_groups']) && $item['entitygroup_wrapper']['old_groups'] != '_new_') {
          $groups['_old_'] = (array) $item['entitygroup_wrapper']['old_groups'];
        }
      }
      $item['groups'] = implode(',', $groups['_new_']) . ',' . implode(',', $groups['_old_']);
    }
    return $values;
  }

}
