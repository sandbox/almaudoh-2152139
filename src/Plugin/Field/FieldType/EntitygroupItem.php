<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Plugin\Field\FieldType\EntitygroupItem.
 */

namespace Drupal\entitygroup\Plugin\Field\FieldType;

use Drupal\Component\Utility\String;
use Drupal\Core\Access\AccessException;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\entitygroup\Entity\Entitygroup;
use Drupal\entitygroup\Entity\EntitygroupType;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

/**
 * Plugin implementation of the 'entitygroup' field type.
 *
 * @FieldType(
 *   id = "entitygroups",
 *   label = @Translation("Entitygroup"),
 *   description = @Translation("This field stores the entitygroups that an entity belongs to."),
 *   default_widget = "entitygroup_text",
 *   default_formatter = "entitygroup_list"
 * )
 */
class EntitygroupItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field) {
    return array(
      'columns' => array(
        'groups' => array(
          'type' => 'text',
          'size' => 'small',
          'not null' => FALSE,
        ),
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return array(
      'entitygroup_type' => '',
    ) + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['groups'] = DataDefinition::create('string')
      ->setLabel(t('Groups'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $groups = trim($this->groups, ',');
    return empty($groups);
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $options = array(t('-- Select entity group type --'));
    foreach (EntitygroupType::loadMultiple() as $name => $type) {
      if (empty($type->restrict) || in_array($this->getEntity()->getEntityTypeId(), $type->restrict)) {
        $options[$name] = $type->label();
      }
    }
    $element['#element_validate'][] = [$this, 'validateEntitygroupType'];
    $element['entitygroup_type'] = array(
      '#type' => 'select',
      '#title' => t('Entitygroup type'),
      '#options' => $options,
      '#default_value' => $this->getSetting('entitygroup_type'),
      '#required' => TRUE,
      '#description' => t('The entitygroup type associated with this field.'),
      //'#description' => t('The entitygroup type for this group. This determines access controls for this group.'),
    );
    return $element;
  }

  /**
   * Validates that an entitygroup type is selected before submitting the form.
   */
  public function validateEntitygroupType(array $element, FormStateInterface $form_state, array $form) {
    if (!$form_state->getValue(['field_storage', 'settings','entitygroup_type'])) {
      if (\Drupal::currentUser()->hasPermission('administer entitygroups')) {
        $args = [
          '!create_link' => \Drupal::l('create an entitygroup here', new Url('entitygroup.type_add')),
        ];
        $form_state->setErrorByName(implode('][', $element['#parents']) . '][entitygroup_type',
          $this->t('Entitygroup type needs to be selected. You can !create_link.', $args));
      }
      else {
        $form_state->setErrorByName(implode('][', $element['#parents']) . '][entitygroup_type',
          $this->t('Entitygroup type needs to be selected.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element['multiselect'] = array(
      '#type' => 'checkbox',
//      '#title' => t('Allow to belong to multiple groups.'),
      '#title' => t('Allow entities of this type to belong to multiple groups.'),
//      '#description' => t('Allow entities of this type to belong to multiple groups.'),
      '#default_value' => $this->getSetting('multiselect'),
      '#required' => FALSE,
    );
    $element['allow_add'] = array(
      '#type' => 'checkbox',
      '#title' => t('Allow new groups to be created like tags by typing into textboxes.'),
      '#default_value' => $this->getSetting('allow_add'),
      '#required' => FALSE,
    );
    return $element;
  }

  /**
   * {@inheritdoc}
   *
   * Create any new entitygroups defined on the fly. And replace group names
   * with group IDs.
   */
  public function preSave() {
    /** @var \Drupal\entitygroup\Entity\EntitygroupType $type */
    $type = EntitygroupType::load($this->getSetting('entitygroup_type'));
    if (!EntitygroupAccessCheck::checkBundleWithPermission($type, 'add entitygroup')) {
      throw new AccessException(sprintf('Access denied to entitygroup %s', $type->name));
    }
    $groups = entitygroup_tags_to_ids($this->groups, $type->name);
    $new_groups= $groups[0];
    unset($groups[0]);
    if ($this->getSetting('allow_add') && is_array($new_groups)) {
      $added = array();
      $not_added = array();
      foreach ($new_groups as $new_tag) {
        $new_group = Entitygroup::create([
          'name'        => $new_tag,
          // @todo: Can we inject this?
          'uid'         => \Drupal::currentUser()->id(),
          'type'        => $type->name,
          'description' => $new_tag,
        ]);
        $status = $new_group->save();
        if ($status == SAVED_NEW) {
          $added[] = $new_tag;
          $groups[$new_group->id()] = $new_tag;
        }
        else {
          $not_added[] = $new_tag;
        }
      }

      $t_args = array(
        '@added'     => implode('", "', $added),
        '@not_added' => implode('", "', $not_added),
        '%type'      => $type->label(),
      );
      if ($added) {
        drupal_set_message(t('New %type "@added" successfully added.', $t_args));
      }
      if ($not_added) {
        drupal_set_message(t('Could not save new %type "@not_added"', $t_args), 'warning');
      }
    }
    $this->set('groups', $groups ? '|' . implode('|', array_keys($groups)) . '|' : '');
  }

}
