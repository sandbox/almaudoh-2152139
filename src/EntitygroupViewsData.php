<?php

/**
 * @file
 * Contains \Drupal\entitygroup\EntitygroupViewsData
 */

namespace Drupal\entitygroup;

use Drupal\views\EntityViewsDataInterface;

/**
 * Provides the views data integration for the entitygroup entity type.
 */
class EntitygroupViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    // Define the base group of this table. Fields that don't have a group defined
    // will go into this field by default.
    $data['entitygroup']['table']['group'] = t('Entitygroup');

    // Define this as a base table – a table that can be described in itself by
    // views (and not just being brought in as a relationship).
    $data['entitygroup']['table']['base'] = array(
      'field' => 'egid', // This is the identifier field for the view.
      'title' => t('Entitygroup'),
      'help' => t('Contains the entitygroups that have been created.'),
      'weight' => -10,
    );
    $data['entitygroup']['table']['entity type'] = 'entitygroup';

    // This table references the {users} table. The declaration below creates an
    // 'implicit' relationship to the entitygroup table, so that when 'entitygroup'
    // is the base table, the fields are automatically available.
//    $data['entitygroup']['table']['join'] = array(
//      // Index this array by the table name to which this table refers.
//      // 'left_field' is the primary key in the referenced table.
//      // 'field' is the foreign key in this table.
//      'users' => array(
//        'left_field' => 'uid',
//        'field' => 'uid',
//      ),
//    );

    // Entitygroup ID
    $data['entitygroup']['egid'] = array(
      'title' => t('Egid'),
      'help' => t('The entitygroup ID.'),
      'field' => array(
        'id' => 'entitygroup',
      ),
      'argument' => array(
        'id' => 'entitygroup_egid',
        'name field' => 'title',
        'numeric' => TRUE,
        'validate type' => 'egid',
      ),
      'filter' => array(
        'id' => 'numeric',
      ),
      'sort' => array(
        'id' => 'standard',
      ),
    );

    // User ID for owner of entitygroup.
    $data['entitygroup']['uid'] = array(
      'title' => t('Owner'),
      'help' => t('The user who created this entitygroup.'),
      'field' => array(
        'id' => 'numeric',
      ),
      'relationship' => array(
        'base' => 'users', // The name of the table to join with
        'field' => 'uid', // The name of the field to join with
        'id' => 'standard',
        'label' => t('Owner'),
      ),
    );

    // Entitygroup type field.
    $data['entitygroup']['type'] = array(
      'title' => t('Entitygroup type'),
      'help' => t('The type of entitygroup.'),
      'field' => array(
        'id' => 'standard',
      ),
      'sort' => array(
        'id' => 'standard',
      ),
      'filter' => array(
        'id' => 'string',
      ),
      'argument' => array(
        'id' => 'string',
      ),
    );

    // Entitygroup name field.
    $data['entitygroup']['name'] = array(
      'title' => t('Entitygroup name'),
      'help' => t('The human-readable name for this entitygroup.'),
      'field' => array(
        'id' => 'standard',
      ),
      'sort' => array(
        'id' => 'standard',
      ),
      'filter' => array(
        'id' => 'string',
      ),
      'argument' => array(
        'id' => 'string',
      ),
    );

    // Entitygroup description field.
    $data['entitygroup']['description'] = array(
      'title' => t('Entitygroup description'),
      'help' => t('A description given to this entitygroup.'),
      'field' => array(
        'id' => 'standard',
      ),
      'sort' => array(
        'id' => 'standard',
      ),
      'filter' => array(
        'id' => 'string',
      ),
      'argument' => array(
        'id' => 'string',
      ),
    );

    // Entitygroup bulk operations form
//    $data['entitygroup']['entitygroup_bulk_form'] = array(
//      'title' => t('Entitygroup operations bulk form'),
//      'help' => t('Add a form element that lets you run operations on multiple entitygroups.'),
//      'field' => array(
//        'id' => 'entitygroup_bulk_form',
//      ),
//    );

    return $data;
  }
}