<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Entity\EntitygroupType.
 */

namespace Drupal\entitygroup\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Entitygroup type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "entitygrouptype",
 *   label = @Translation("Entitygroup type"),
 *   handlers = {
 *     "storage" = "Drupal\Core\Config\Entity\ConfigEntityStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entitygroup\EntitygroupTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entitygroup\Form\EntitygroupTypeForm",
 *       "edit" = "Drupal\entitygroup\Form\EntitygroupTypeForm",
 *       "delete" = "Drupal\entitygroup\Form\EntitygroupTypeDeleteForm"
 *     },
 *   },
 *   admin_permission = "administer entitygroups",
 *   config_prefix = "type",
 *   bundle_of = "entitygroup",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "entitygroup.list",
 *     "edit-form" = "entitygroup.type_edit",
 *     "delete-form" = "entitygroup.type_delete"
 *   }
 * )
 *
 */
class EntitygroupType extends ConfigEntityBundleBase {

  /**
   * The machine name of this entitygroup type.
   *
   * @var string
   */
  public $name;

  /**
   * The UUID of the entitygroup type.
   *
   * @var string
   */
  public $uuid;

  /**
   * The human-readable name of the entitygroup type.
   *
   * @var string
   */
  public $label;

  /**
   * A brief description of this entitygroup type.
   *
   * @var string
   */
  public $description;

  /**
   * Help information shown to the user when creating an Entitygroup of this type.
   *
   * @var string
   */
  public $help;

  /**
   * Determines whether the entitygroup machine name can be changed or not.
   *
   * @var bool
   */
  public $locked = FALSE;

  /**
   * The access control strategy to be used for this entitygroup type.
   *
   * Possible access control modes include:
   * - owner: Only the owner (creator) of the entitygroup has access.
   * - user: A specified list of users (comma-separated) has access.
   * - role: Users in a specified drupal role have access.
   * - group: Users belonging to a specified entitygroup have access.
   * - all: All users including anonymous have access.
   *
   * E.g. for owner access control mode, the following would be specified:
   * @code
   * $access = [
   *   'mode' => 'owner',
   *   'owner' => <uid_of_creator>
   * ];
   * @endcode
   *
   * @code
   * $access = [
   *   'mode' => 'user',
   *   'user' => 'username1,username3,username8',
   * ];
   * @endcode
   *
   * @var string
   */
  public $access = [
    'mode' => '',
    'owner' => '',
    'user' => '',
    'role' => [],
    'group' => '',
    'all' => '',
  ];

  /**
   * The menu settings for this entitygroup type.
   *
   * @var array
   */
  public $menu = [
    'enabled' => FALSE,
    'path' => '',
    'parent' => '',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ];

  /**
   * The entity types that can be members of this group, empty if all entities
   *  are allowed
   *
   * @var array
   */
	public $restrict = array();
  
  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->name;
  }

  /**
   * Gets the menu link properties for this entitygroup type if it exists.
   *
   * @return array
   *   Array containing menu link information
   *
   * @see \Drupal\entitygroup\Plugin\Derivative\EntitygroupMenuLink
   */
  public function getMenuLink() {
    if ($this->menu['enabled']) {
      if (isset($this->menu['parent'])) {
        list($menu_name, $parent) = explode(':', $this->menu['parent'], 2);
      }
      else {
        $menu_name = ''; $parent = '';
      }

      return [
        'title'       => $this->menu['title'],
        'path'        => $this->menu['path'],
        'weight'      => $this->menu['weight'],
        'description' => $this->menu['description'],
        'parent'      => $parent,
        'menu_name'   => $menu_name,
      ];
    }
    else {
      return false;
    }
  }

  /**
   * Updates the menu link properties for this entitygroup type.
   *
   * @param array $new_link_properties
   *   The updates to be made to the men link properties.
   *
   * @return static
   */
  public function updateMenuLink($new_link_properties) {
    foreach (['path', 'parent', 'title', 'weight', 'menu_name', 'description'] as $key) {
      $this->menu[$key] = $new_link_properties[$key];
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storageInterface, array &$entities) {
    foreach ($entities as $id => $entity) {
      $entities[$id]->menu += [
        'enabled' => FALSE,
        'path' => '',
        'parent' => '',
        'title' => '',
        'description' => '',
        'weight' => 0,
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    $this->menu += [
      'enabled' => FALSE,
      'path' => '',
      'parent' => '',
      'title' => '',
      'description' => '',
      'weight' => 0,
    ];
    parent::preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage_controller, array $entities) {
    // @todo Delete all the fields associated with this entitygroup type.
    parent::postDelete($storage_controller, $entities);
  }

  /**
   * Returns the list of possible access modes for entitygroups
   *
   * @return string
   *   Array of allowed access modes for all entitygroup types
   */
  public static function getAccessModes() {
    return array(
      'owner' => t('Only the owner of the entity group has access.'),
      'user' => t('A selected set of users have access.'),
      'role' => t('Users belonging to a drupal role have access.'),
      'group' => t('Members of an entity group have access.'),
      'all' => t('All users of the site have access (including anonymous users).'),
    );
  }

  /**
   * Convenience function for machine_name and other uses.
   */
  public static function typeExists($name) {
    return (bool) EntitygroupType::load($name);
  }
}
