<?php

/**
 * @file
 * Definition of Drupal\entitygroup\Entity\Entitygroup.
 */

namespace Drupal\entitygroup\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;

/**
 * Defines the entitygroup entity class.
 *
 * @ContentEntityType(
 *   id = "entitygroup",
 *   label = @Translation("Entitygroup"),
 *   module = "entitygroup",
 *   handlers = {
 *     "access" = "Drupal\entitygroup\EntitygroupAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entitygroup\EntitygroupListBuilder",
 *     "views_data" = "Drupal\entitygroup\EntitygroupViewsData",
 *     "form" = {
 *       "add" = "Drupal\entitygroup\Form\EntitygroupForm",
 *       "edit" = "Drupal\entitygroup\Form\EntitygroupForm",
 *       "delete" = "Drupal\entitygroup\Form\EntitygroupDeleteForm"
 *     },
 *   },
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   entity_keys = {
 *     "id" = "egid",
 *     "uuid" = "uuid",
 *     "label" = "name",
 *     "bundle" = "type"
 *   },
 *   base_table = "entitygroup",
 *   data_table = "entitygroup_field_data",
 *   permission_granularity = "bundle",
 *   bundle_entity_type = "entitygrouptype",
 *   field_ui_base_route = "entitygroup.type_edit",
 *	 bundle_keys = {
 *     "bundle" = "name",
 *   },
 *   links = {
 *     "canonical" = "entitygroup.view",
 *     "edit-form" = "entitygroup.edit",
 *     "admin-form" = "entitygroup.type_edit"
 *   }
 * )
 *     "storage" = "Drupal\Core\Entity\ConfigStorageController",
 */
class Entitygroup extends ContentEntityBase {

  /**
   * Static cache for the members of this entitygroup.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $members;
  
  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->get('egid')->value;
  }

  /**
   * Gets the member entities in an entitygroup as an array.
   *
   * @return array
   *   Members of the specified entitygroup, grouped into sub-arrays keyed by
   *   the entity type.
   */
  public function getMembers() {
    if (!isset($this->members)) {
      $this->members = $this->pollMembers();
    }
    return $this->members;
  }

  /**
   * Pulls the members of the group attached as fields to other entities.
   *
   * @todo Add caching to this method.
   */
  protected function pollMembers() {
    // Query for entities that have this entitygroup as a field.
    $result = array();
    foreach ($this->entityManager()->getAllBundleInfo() as $entity_type => $bundle_info) {
      try {
        $query = \Drupal::entityQuery($entity_type, 'OR');
        $condition = FALSE;
        foreach ($bundle_info as $bundle => $info) {
          foreach ($this->entityManager()
                     ->getFieldDefinitions($entity_type, $bundle) as $field_name => $field) {
            if ($field->getType() == 'entitygroups' && $field->getSetting('entitygroup_type') == $this->type->value) {
              $query->condition("$field_name.groups", '|' . $this->id() . '|', 'CONTAINS');
              $condition = TRUE;
            }
          }
        }
        if ($condition) {
          $result[$entity_type] = $query->execute();
        }
      }
      catch (\Exception $e) {
        // @todo: Do something with the Exception
      }
    }
    return $result;
  }

  /**
   * {@inheritdoc
   */
  protected function urlRouteParameters($rel) {
    return parent::urlRouteParameters($rel) + [
      $this->getEntityType()->getBundleEntityType() => $this->type->value,
    ];
  }

  /**
   * Generates a base path that is used for form redirect after submit.
   *
   * Due to the custom menu that may be configured, the form needs to be
   * redirected back to same entry point.
   *
   * @return string
   *   The redirect path
   */
  public static function getCurrentBasePath() {
    $regexp = '%/add|/list|(/[0-9]+(/(edit|view|delete))?)%i';
    return preg_replace($regexp, '', current_path());
  }
  
  /**
   * Creates a url object that corresponds to the list route for the current
   * entitygroup.
   *
   * Due to the custom menu that may be configured, the form needs to be
   * redirected back to same entry point.
   *
   * @return \Drupal\Core\Url
   *   The base url which can be used for redirect
   */
  public function getCurrentBaseUrl() {
    return new Url($this->getCurrentBaseRouteName(), ['entitygroup' => $this->id(), 'entitygrouptype' => $this->type->value]);
  }

  /**
   * Creates a url object that corresponds to a specified action route for the
   * current entitygroup.
   *
   * @param string
   *   The action for which Url is sought. Can be 'add', 'edit' or 'delete'.
   *
   * @return \Drupal\Core\Url
   *   The url which corresponds to the specified action.
   */
  public function getUrlForOperation($action) {
    return new Url($this->getCurrentBaseRouteName()."_$action", ['entitygroup' => $this->id(), 'entitygrouptype' => $this->type->value]);
  }

  /**
   * Returns a route name corresponding to the base path of this entitygroup.
   *
   * @return string
   *
   * @throws \RuntimeException
   */
  protected function getCurrentBaseRouteName() {
    $route_name = $this->request()->get(RouteObjectInterface::ROUTE_NAME);
    if (in_array($route_name, array('entitygroup.add', 'entitygroup.view', 'entitygroup.edit', 'entitygroup.delete'))) {
      return 'entitygroup.list';
    }
    else if (preg_match('%(entitygroup\.[_a-zA-Z0-9])_add|_view|_edit|_delete%i', $route_name, $matches)) {
      return $matches[1];
    }
    else {
      throw new \RuntimeException('No Entitygroup associated with the current route.');
    }
  }

  /**
   * Wraps the request object.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   The currently active request object.
   */
  protected function request() {
    return \Drupal::request();
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    
    // Initialize the entitygroup type if not specified.
    // @todo Is there a better way that fits all use cases?
    if (!isset($values['type'])) {
      $type = \Drupal::request()->get('entitygrouptype');
      if (is_object($type)) $type = $type->name;

      $values['type'] = $type;
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['egid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Entitygroup ID'))
      ->setDescription(t('The entitygroup ID.'))
      ->setReadOnly(TRUE);
    
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The user UUID.'))
      ->setReadOnly(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User ID'))
      ->setDescription(t('The user that created and owns this entity group.'))
      ->setSettings(array(
        'target_type' => 'user',
        'default_value' => 0,
      ));

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity Group Type'))
      ->setDescription(t('The entitygroup type for separating entity groups used for different purposes.'))
      ->setSetting('default_value', '')
      ->setPropertyConstraints('value', array('Length' => array('max' => 100)));

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The human-readable name of this entitygroup. Must be unique.'))
      ->setSetting('default_value', '')
      ->setPropertyConstraints('value', array('Length' => array('max' => 255)));
        // @todo implement Constraint and ConstraintValidator for entityname uniqueness
        // @see \Drupal\user\Plugin\Validation\Constraint\UserNameUnique

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('A more elaborate description of the function of the entitygroup.'))
      ->setSetting('default_value', '')
      ->setPropertyConstraints('value', array('Length' => array('max' => 255)));

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The user language code.'));

    return $fields;
  }

  /**
   * Loads entity groups that have a specific type name.
   *
   * @param string $type_name
   *   The name of the type of entitygroup to load.
   *
   * @return \Drupal\entitygroup\Entity\Entitygroup[]
   */
  public static function loadByType($type_name) {
    $result = \Drupal::entityQuery('entitygroup')
      ->condition('type', $type_name)
      ->execute();
    return static::loadMultiple($result);
  }

}

