<?php

/**
 * @file
 * Contains \Drupal\entitygroup\EntitygroupAccessControlHandler
 */

namespace Drupal\entitygroup;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entitygroup\Access\EntitygroupAccessCheck;
use Drupal\node\Plugin\views\filter\Access;

/**
 * Access controller for the Entitygroup entity.
 *
 * @see \Drupal\entitygroup\Entity\Entitygroup.
 */
class EntitygroupAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, $langcode, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view entitygroup')->andIf(EntitygroupAccessCheck::access($entity));

      case 'update':
          return AccessResult::allowedIfHasPermission($account, 'edit entitygroup')->andIf(EntitygroupAccessCheck::access($entity));

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete entitygroup')->andIf(EntitygroupAccessCheck::access($entity));
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add entitygroup')->andIf(EntitygroupAccessCheck::accessBundle($entity_bundle));
  }

}
