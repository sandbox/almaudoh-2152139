<?php

/**
 * @file
 * Contains \Drupal\entitygroup\Routing\EntitygroupCustomRoutes.
 */

namespace Drupal\entitygroup\Routing;

use Drupal\entitygroup\Entity\EntitygroupType;
use Symfony\Component\Routing\Route;

/**
 * Listens to the dynamic route events.
 */
class EntitygroupCustomRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    // Generate routes for the entitygroup types with custom menu.
    $routes = array();
    foreach (EntitygroupType::loadMultiple() as $type) {
  		if ($type->menu['enabled']) {
        // Custom add, edit, delete menu items for entitygroups
  			$base = !empty($type->menu['path']) ? $type->menu['path'] : $type->name;
  			$base = trim(check_url($base), '/');

  			// Add entitygroup type base route
  			$routes["entitygroup.{$type->name}"] = new Route($base, array(
            '_entity_list' => 'entitygroup',
            'entitygrouptype' => $type->name,
            '_title_callback' => 'Drupal\entitygroup\EntitygroupController::entitygroupTitle',
            'action' => 'list group',
  			  ),
  			  array('_entitygroup_access' => 'list', '_permission' => 'view entitygroup'),
          array('_access_mode' => 'ALL')
  			);
  			
  			// Entitygroup create route
  			$routes["entitygroup.{$type->name}_add"] = new Route("$base/add", array(
            '_entity_form' => 'entitygroup.add',
            'entitygrouptype' => $type->name,
            '_title_callback' => 'Drupal\entitygroup\EntitygroupController::entitygroupTitle',
            'action' => 'add group',
  			  ),
          array('_entitygroup_access' => 'add', '_permission' => 'add entitygroup'),
          // @todo options to do BOTH access checks
          array('_access_mode' => 'ALL')
  			);
  			
  			// Entitygroup view route
  			$routes["entitygroup.{$type->name}_view"] = new Route("$base/{entitygroup}", array(
            '_entity_view' => 'entitygroup',
            'entitygrouptype' => $type->name,
            '_title_callback' => 'Drupal\entitygroup\EntitygroupController::entitygroupTitle',
            'action' => 'view group',
  			  ),
          array('_entitygroup_access' => 'view', '_permission' => 'edit entitygroup'),
          // @todo options to do BOTH access checks
          array('_access_mode' => 'ALL')
  			);

        // Entitygroup edit route
  			$routes["entitygroup.{$type->name}_edit"] = new Route("$base/{entitygroup}/edit", array(
            '_entity_form' => 'entitygroup.edit',
            'entitygrouptype' => $type->name,
            '_title_callback' => 'Drupal\entitygroup\EntitygroupController::entitygroupTitle',
            'action' => 'edit group',
  			  ),
          array('_entitygroup_access' => 'edit', '_permission' => 'edit entitygroup'),
          // @todo options to do BOTH access checks
          array('_access_mode' => 'ALL')
  			);
  			
  			// Add entitygroup delete route
  			$routes["entitygroup.{$type->name}_delete"] = new Route("$base/{entitygroup}/delete", array(
            '_entity_form' => 'entitygroup.delete',
            'entitygrouptype' => $type->name,
            '_title_callback' => 'Drupal\entitygroup\EntitygroupController::entitygroupTitle',
            'action' => 'delete group',
  			  ),
          array('_entitygroup_access' => 'delete', '_permission' => 'delete entitygroup'),
          // @todo options to do BOTH access checks
          array('_access_mode' => 'ALL')
        );
  		}
    }
    return $routes;
  }
}
